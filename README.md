Novo Site Serramar
==================


Justificativas do Projeto
-----------------------------
A Cooperativa possui um site bastante desatualizado em relação ao mercado. A empresa é líder de mercado com alguns produtos e não possui sites que represente essas marcas.

Objetivos
-----------------------------
•	Aprofundar envolvimento sobre a identidade da empresa
•	Defender novo posicionamento da marca;
•	Servi de sustentação para campanha de em outras mídias.

Resultados esperados
-----------------------------
•	Participação ativa dos cliente e distribuidores nas áreas de integração do site.
•	Reconhecimento da presença digital da marca.

Premissas
-----------------------------
•	A Tecnologia a ser utilizada é PHP (CakePHP 2.1) juntamente com MySQL como gerenciador de Banco de Dados.
•	O Painel de controle do site deve permitir diferentes níveis de acesso, pois será acessados por usuários com diferentes níveis de experiência com internet.
•	O layout do site deve seguir a linha criativa já utilizadas na campanha.


Ondasete Mkt. Com.