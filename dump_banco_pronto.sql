-- phpMyAdmin SQL Dump
-- version 3.3.10.4
-- http://www.phpmyadmin.net
--
-- Servidor: mysql.aprovacao.ondasete.com.br
-- Tempo de Geração: Abr 16, 2013 as 10:51 AM
-- Versão do Servidor: 5.1.39
-- Versão do PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Banco de Dados: `serramar`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=149 ;

--
-- Extraindo dados da tabela `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 190),
(2, 1, NULL, NULL, 'Farms', 2, 17),
(8, 1, NULL, NULL, 'Groups', 18, 29),
(14, 1, NULL, NULL, 'Information', 30, 41),
(20, 1, NULL, NULL, 'Newspappers', 42, 55),
(26, 1, NULL, NULL, 'Orders', 56, 67),
(38, 1, NULL, NULL, 'Pages', 68, 71),
(39, 38, NULL, NULL, 'display', 69, 70),
(40, 1, NULL, NULL, 'Products', 72, 97),
(46, 1, NULL, NULL, 'Users', 98, 115),
(47, 46, NULL, NULL, 'login', 99, 100),
(48, 46, NULL, NULL, 'logout', 101, 102),
(54, 1, NULL, NULL, 'Acl', 116, 171),
(55, 54, NULL, NULL, 'Acl', 117, 122),
(56, 55, NULL, NULL, 'index', 118, 119),
(57, 55, NULL, NULL, 'admin_index', 120, 121),
(58, 54, NULL, NULL, 'Acos', 123, 134),
(59, 58, NULL, NULL, 'admin_index', 124, 125),
(60, 58, NULL, NULL, 'admin_empty_acos', 126, 127),
(61, 58, NULL, NULL, 'admin_build_acl', 128, 129),
(62, 58, NULL, NULL, 'admin_prune_acos', 130, 131),
(63, 58, NULL, NULL, 'admin_synchronize', 132, 133),
(64, 54, NULL, NULL, 'Aros', 135, 170),
(65, 64, NULL, NULL, 'admin_index', 136, 137),
(66, 64, NULL, NULL, 'admin_check', 138, 139),
(67, 64, NULL, NULL, 'admin_users', 140, 141),
(68, 64, NULL, NULL, 'admin_update_user_role', 142, 143),
(69, 64, NULL, NULL, 'admin_ajax_role_permissions', 144, 145),
(70, 64, NULL, NULL, 'admin_role_permissions', 146, 147),
(71, 64, NULL, NULL, 'admin_user_permissions', 148, 149),
(72, 64, NULL, NULL, 'admin_empty_permissions', 150, 151),
(73, 64, NULL, NULL, 'admin_clear_user_specific_permissions', 152, 153),
(74, 64, NULL, NULL, 'admin_grant_all_controllers', 154, 155),
(75, 64, NULL, NULL, 'admin_deny_all_controllers', 156, 157),
(76, 64, NULL, NULL, 'admin_get_role_controller_permission', 158, 159),
(77, 64, NULL, NULL, 'admin_grant_role_permission', 160, 161),
(78, 64, NULL, NULL, 'admin_deny_role_permission', 162, 163),
(79, 64, NULL, NULL, 'admin_get_user_controller_permission', 164, 165),
(80, 64, NULL, NULL, 'admin_grant_user_permission', 166, 167),
(81, 64, NULL, NULL, 'admin_deny_user_permission', 168, 169),
(89, 2, NULL, NULL, 'admin_index', 3, 4),
(90, 2, NULL, NULL, 'admin_view', 5, 6),
(91, 2, NULL, NULL, 'admin_add', 7, 8),
(92, 2, NULL, NULL, 'admin_edit', 9, 10),
(93, 2, NULL, NULL, 'admin_delete', 11, 12),
(94, 8, NULL, NULL, 'admin_index', 19, 20),
(95, 8, NULL, NULL, 'admin_view', 21, 22),
(96, 8, NULL, NULL, 'admin_add', 23, 24),
(97, 8, NULL, NULL, 'admin_edit', 25, 26),
(98, 8, NULL, NULL, 'admin_delete', 27, 28),
(99, 14, NULL, NULL, 'admin_index', 31, 32),
(100, 14, NULL, NULL, 'admin_view', 33, 34),
(101, 14, NULL, NULL, 'admin_add', 35, 36),
(102, 14, NULL, NULL, 'admin_edit', 37, 38),
(103, 14, NULL, NULL, 'admin_delete', 39, 40),
(104, 20, NULL, NULL, 'admin_index', 43, 44),
(105, 20, NULL, NULL, 'admin_view', 45, 46),
(106, 20, NULL, NULL, 'admin_add', 47, 48),
(107, 20, NULL, NULL, 'admin_edit', 49, 50),
(108, 20, NULL, NULL, 'admin_delete', 51, 52),
(109, 26, NULL, NULL, 'admin_view', 57, 58),
(110, 26, NULL, NULL, 'admin_add', 59, 60),
(111, 26, NULL, NULL, 'admin_edit', 61, 62),
(112, 26, NULL, NULL, 'admin_delete', 63, 64),
(118, 40, NULL, NULL, 'admin_index', 73, 74),
(119, 40, NULL, NULL, 'admin_view', 75, 76),
(120, 40, NULL, NULL, 'admin_add', 77, 78),
(121, 40, NULL, NULL, 'admin_edit', 79, 80),
(122, 40, NULL, NULL, 'admin_delete', 81, 82),
(123, 46, NULL, NULL, 'admin_index', 103, 104),
(124, 46, NULL, NULL, 'admin_view', 105, 106),
(125, 46, NULL, NULL, 'admin_add', 107, 108),
(126, 46, NULL, NULL, 'admin_edit', 109, 110),
(127, 46, NULL, NULL, 'admin_delete', 111, 112),
(128, 1, NULL, NULL, 'TwitterBootstrap', 172, 177),
(129, 128, NULL, NULL, 'TwitterBootstrap', 173, 176),
(130, 129, NULL, NULL, 'index', 174, 175),
(131, 26, NULL, NULL, 'admin_index', 65, 66),
(132, 1, NULL, NULL, 'OrdersItems', 178, 189),
(133, 132, NULL, NULL, 'admin_index', 179, 180),
(134, 132, NULL, NULL, 'admin_view', 181, 182),
(135, 132, NULL, NULL, 'admin_add', 183, 184),
(136, 132, NULL, NULL, 'admin_edit', 185, 186),
(137, 132, NULL, NULL, 'admin_delete', 187, 188),
(138, 46, NULL, NULL, 'admin_painel', 113, 114),
(139, 40, NULL, NULL, 'index', 83, 84),
(140, 2, NULL, NULL, 'index', 13, 14),
(141, 2, NULL, NULL, 'view', 15, 16),
(142, 40, NULL, NULL, 'serramar', 85, 86),
(143, 40, NULL, NULL, 'maringa', 87, 88),
(144, 40, NULL, NULL, 'milkmix', 89, 90),
(145, 20, NULL, NULL, 'index', 53, 54),
(146, 40, NULL, NULL, 'serramar_all', 91, 92),
(147, 40, NULL, NULL, 'milkmix_all', 93, 94),
(148, 40, NULL, NULL, 'maringa_all', 95, 96);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 6),
(2, 1, 'User', 1, NULL, 2, 3),
(3, NULL, 'Group', 2, NULL, 7, 8),
(4, NULL, 'Group', 3, NULL, 9, 10),
(5, NULL, 'Group', 4, NULL, 11, 36),
(8, 5, 'User', 4, NULL, 12, 13),
(9, 5, 'User', 5, NULL, 14, 15),
(10, 5, 'User', 6, NULL, 16, 17),
(11, 5, 'User', 7, NULL, 18, 19),
(12, 5, 'User', 8, NULL, 20, 21),
(13, 5, 'User', 9, NULL, 22, 23),
(14, 5, 'User', 10, NULL, 24, 25),
(15, 5, 'User', 11, NULL, 26, 27),
(16, 5, 'User', 12, NULL, 28, 29),
(17, 5, 'User', 13, NULL, 30, 31),
(18, 5, 'User', 14, NULL, 32, 33),
(19, 5, 'User', 15, NULL, 34, 35),
(20, 1, 'User', 16, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95 ;

--
-- Extraindo dados da tabela `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 1, '1', '1', '1', '1'),
(2, 3, 91, '1', '1', '1', '1'),
(3, 3, 93, '1', '1', '1', '1'),
(4, 3, 92, '1', '1', '1', '1'),
(5, 3, 89, '1', '1', '1', '1'),
(6, 3, 90, '1', '1', '1', '1'),
(7, 3, 101, '1', '1', '1', '1'),
(8, 3, 103, '1', '1', '1', '1'),
(9, 3, 102, '1', '1', '1', '1'),
(10, 3, 99, '1', '1', '1', '1'),
(11, 3, 100, '1', '1', '1', '1'),
(12, 3, 106, '1', '1', '1', '1'),
(13, 3, 108, '1', '1', '1', '1'),
(14, 3, 107, '1', '1', '1', '1'),
(15, 3, 104, '1', '1', '1', '1'),
(16, 3, 105, '1', '1', '1', '1'),
(17, 4, 110, '-1', '-1', '-1', '-1'),
(18, 4, 112, '1', '1', '1', '1'),
(19, 4, 111, '1', '1', '1', '1'),
(20, 4, 131, '1', '1', '1', '1'),
(21, 4, 109, '1', '1', '1', '1'),
(27, 3, 39, '1', '1', '1', '1'),
(28, 4, 39, '1', '1', '1', '1'),
(33, 3, 120, '1', '1', '1', '1'),
(34, 3, 122, '1', '1', '1', '1'),
(35, 3, 121, '1', '1', '1', '1'),
(36, 3, 118, '1', '1', '1', '1'),
(37, 3, 119, '1', '1', '1', '1'),
(38, 3, 48, '1', '1', '1', '1'),
(39, 3, 47, '1', '1', '1', '1'),
(40, 4, 47, '1', '1', '1', '1'),
(41, 4, 48, '1', '1', '1', '1'),
(42, 4, 120, '1', '1', '1', '1'),
(43, 4, 122, '1', '1', '1', '1'),
(44, 4, 121, '1', '1', '1', '1'),
(45, 4, 118, '1', '1', '1', '1'),
(46, 4, 119, '1', '1', '1', '1'),
(47, 4, 125, '-1', '-1', '-1', '-1'),
(48, 4, 101, '1', '1', '1', '1'),
(49, 4, 103, '1', '1', '1', '1'),
(50, 4, 102, '1', '1', '1', '1'),
(51, 4, 99, '1', '1', '1', '1'),
(52, 4, 100, '1', '1', '1', '1'),
(53, 4, 130, '1', '1', '1', '1'),
(54, 3, 130, '1', '1', '1', '1'),
(55, 5, 110, '1', '1', '1', '1'),
(56, 5, 112, '1', '1', '1', '1'),
(57, 5, 111, '1', '1', '1', '1'),
(58, 5, 131, '1', '1', '1', '1'),
(59, 5, 109, '1', '1', '1', '1'),
(60, 5, 135, '1', '1', '1', '1'),
(61, 5, 137, '1', '1', '1', '1'),
(62, 5, 136, '1', '1', '1', '1'),
(63, 5, 133, '1', '1', '1', '1'),
(64, 5, 134, '1', '1', '1', '1'),
(65, 5, 39, '1', '1', '1', '1'),
(66, 5, 118, '-1', '-1', '-1', '-1'),
(67, 5, 119, '-1', '-1', '-1', '-1'),
(68, 5, 47, '1', '1', '1', '1'),
(69, 5, 48, '1', '1', '1', '1'),
(70, 4, 138, '1', '1', '1', '1'),
(71, 3, 138, '1', '1', '1', '1'),
(72, 4, 139, '1', '1', '1', '1'),
(73, 3, 139, '1', '1', '1', '1'),
(74, 5, 139, '1', '1', '1', '1'),
(75, 5, 141, '1', '1', '1', '1'),
(76, 5, 140, '1', '1', '1', '1'),
(77, 3, 140, '1', '1', '1', '1'),
(78, 3, 141, '1', '1', '1', '1'),
(79, 4, 140, '1', '1', '1', '1'),
(80, 4, 141, '1', '1', '1', '1'),
(81, 5, 130, '1', '1', '1', '1'),
(82, 4, 143, '1', '1', '1', '1'),
(83, 4, 144, '1', '1', '1', '1'),
(84, 4, 142, '1', '1', '1', '1'),
(85, 3, 125, '-1', '-1', '-1', '-1'),
(86, 3, 142, '1', '1', '1', '1'),
(87, 3, 144, '1', '1', '1', '1'),
(88, 3, 143, '1', '1', '1', '1'),
(89, 5, 142, '1', '1', '1', '1'),
(90, 5, 144, '1', '1', '1', '1'),
(91, 5, 143, '1', '1', '1', '1'),
(92, 5, 145, '1', '1', '1', '1'),
(93, 3, 145, '1', '1', '1', '1'),
(94, 4, 145, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `farms`
--

CREATE TABLE IF NOT EXISTS `farms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `picture` varchar(255) NOT NULL,
  `path_picture` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `farms`
--

INSERT INTO `farms` (`id`, `title`, `body`, `picture`, `path_picture`, `slug`, `created`, `updated`) VALUES
(3, 'Fazendas Serramar', '<p class="western" align="JUSTIFY"><em>Aqui voc&ecirc; vai conhecer algumas das fazendas que representam o alto grau de excel&ecirc;ncia dos nossos produtores.</em></p>', 'fazenda_joao.jpg', '3', 'fazenda_do_sr_joao', '2012-11-19 17:10:29', '2013-04-15 20:23:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT '	',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `updated`) VALUES
(1, 'Administrador', '2012-10-01 11:10:12', '2012-10-01 11:10:12'),
(2, 'Gerente', '2012-10-01 15:08:18', '2012-10-01 15:08:18'),
(3, 'Cooperativa', '2012-10-01 15:08:28', '2012-10-01 15:08:28'),
(4, 'Varejistas', '2012-10-01 16:46:02', '2012-10-01 16:46:02');

-- --------------------------------------------------------

--
-- Estrutura da tabela `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item` varchar(45) NOT NULL,
  `reference_value` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  `created` varchar(45) DEFAULT NULL,
  `update` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `information`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `newspappers`
--

CREATE TABLE IF NOT EXISTS `newspappers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `code` mediumtext NOT NULL,
  `picture` varchar(255) NOT NULL,
  `picture_path` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `newspappers`
--

INSERT INTO `newspappers` (`id`, `title`, `code`, `picture`, `picture_path`, `created`) VALUES
(1, 'EdiÃ§Ã£o #1', '001', 'capa_o_cooperador_ed_653.jpg', '1', '2012-10-04 16:00:14'),
(2, 'EdiÃ§Ã£o #2', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'capa_o_cooperador_ed_652.jpg', '2', '2012-10-10 17:07:27'),
(3, 'EdiÃ§Ã£o #3', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'capa_o_cooperador_ed_651.jpg', '3', '2012-10-10 17:11:54'),
(4, 'EdiÃ§Ã£o #3', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr.', 'capa_o_cooperador_ed_650.jpg', '4', '2012-10-10 17:16:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `total` decimal(10,2) DEFAULT NULL COMMENT '		',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `orders`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `ordersItems`
--

CREATE TABLE IF NOT EXISTS `ordersItems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `ordersItems`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `storage` varchar(45) DEFAULT NULL,
  `temperature` varchar(45) DEFAULT NULL,
  `expiration` varchar(45) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `picture` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `unit` varchar(45) NOT NULL,
  `package` varchar(45) DEFAULT NULL,
  `portions` varchar(45) DEFAULT NULL,
  `line` varchar(60) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `storage`, `temperature`, `expiration`, `price`, `picture`, `path`, `unit`, `package`, `portions`, `line`, `created`, `updated`) VALUES
(7, 'Leite Pasteurizado Tipo B', '', '', '', '', NULL, 'leite_pasteurizado_tipo_B_integral_2_min_MG_0022.png', '7', '1', '', '', 'serramar', '2012-11-08 13:59:47', '1352399067'),
(8, 'Leite Pasteurizado Tipo B', '', '', '', '', NULL, 'leite_pasteurizado_tipo_B_integral_min_MG_0022.png', '8', '1', '', '', 'serramar', '2012-11-08 14:11:24', '1352399090'),
(9, 'Doce de Leite', '', '', '', '', NULL, 'doce_de_leite_min_MG_9967.png', '9', '1', '', '', 'serramar', '2012-11-08 14:16:30', '1352399100'),
(10, 'Coalhada', '', '', '', '', NULL, 'coalhada_natural_min_MG_9993.png', '10', '1', '', '', 'serramar', '2012-11-08 14:24:12', '1352399109'),
(11, 'Suco de maÃ§a e uva', '', '', '', '', NULL, 'suco_maca_uva_min_MG_9970.png', '11', '1', '', '', 'serramar', '2012-11-08 14:24:56', '1352399119'),
(12, 'Suco de laranja', '', '', '', '', NULL, 'suco_laranja_450ml_min_MG_9970.png', '12', '1', '', '', 'serramar', '2012-11-08 14:25:28', '1352399133'),
(13, 'Manteiga extra com sal', '', '', '', '', NULL, 'manteiga_extra_com_sal_200g_min_MG_9965.png', '13', '1', '', '', 'serramar', '2012-11-08 14:26:08', '1352399143'),
(14, 'Milk Mix Ameixa', '', '', '', '', NULL, 'milk_mix_ameixa_min_MG_9985.png', '14', '1', '', '', 'milkmix', '2012-11-08 14:36:24', '1352399161'),
(15, 'Milk Mix Coco Light', '', '', '', '', NULL, 'milk_mix_coco_light_min_MG_9990.png', '15', '1', '', '', 'milkmix', '2012-11-08 14:37:52', '1352399184'),
(16, 'Milk Mix Laranja', '', '', '', '', NULL, 'milk_mix_laranja_pequeno_min.png', '16', '1', '', '', 'milkmix', '2012-11-08 14:38:21', '1352399200'),
(17, 'Milk Mix Polpas de Frutas e Cereal', '', '', '', '', NULL, 'milk_mix_mamÃ£o_banana_maca_cereal_light_2_min_MG_9990.png', '17', '1', '', '', 'milkmix', '2012-11-08 14:40:42', '1352399346'),
(18, 'Milk Mix Polpas de Frutas e Cereal Light', '', '', '', '', NULL, 'milk_mix_mamÃ£o_banana_maca_cereal_light_min_MG_9990.png', '18', '1', '', '', 'milkmix', '2012-11-08 14:41:52', '1352399379'),
(19, 'Milk Mix Morango', '', '', '', '', NULL, 'milk_mix_morango_light_min_2_MG_9990.png', '19', '1', '', '', 'milkmix', '2012-11-08 14:42:53', '1352399451'),
(21, 'Milk Mix Morango Light', '', '', '', '', NULL, 'milk_mix_morango_light_min_MG_9990.png', '21', '1', '', '', 'milkmix', '2012-11-08 14:44:43', '1352399480'),
(22, 'Milk Mix Pesego Light Grande', '', '', '', '', NULL, 'milk_mix_pesego_light_min_MG_9990.png', '22', '1', '', '', 'milkmix', '2012-11-08 14:45:37', '1352393137'),
(23, 'Leite Tipo B', '', '', '', '', NULL, 'leite_tipo_B_integral_homogeneizado_min_MG_0022.png', '23', '1', '', '', 'maringa', '2012-11-08 14:50:22', '1352401618'),
(24, 'Manteiga Extra com Sal', '', '', '', '', NULL, 'manteiga_extra_com_sal_min_MG_9966.png', '24', '1', '', '', 'maringa', '2012-11-08 14:50:49', '1352401894');

-- --------------------------------------------------------

--
-- Estrutura da tabela `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `schema_migrations`
--

INSERT INTO `schema_migrations` (`id`, `class`, `type`, `created`) VALUES
(1, 'InitMigrations', 'Migrations', '2012-10-02 17:41:05'),
(2, 'ConvertVersionToClassNames', 'Migrations', '2012-10-02 17:41:05'),
(3, 'IncreaseClassNameLength', 'Migrations', '2012-10-02 17:41:05'),
(4, 'Develop', 'app', '2012-10-02 17:42:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(128) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `username`, `password`, `group_id`, `created`) VALUES
(1, 'Ricardo', 'web@ondasete.com.br', 'admin', '4b81276b469eeba509fe14f1f7799ed2505b5f39', 1, '2012-10-01 11:30:31'),
(11, 'Varejista01', 'teste@teste.com', 'teste', '64fbe672e40522debb31a17595278f83c3b83bb6', 4, '2012-10-29 15:56:59'),
(12, 'Teste01', 'teste@teste.com.br', 'teste', '64fbe672e40522debb31a17595278f83c3b83bb6', 4, '2012-10-29 16:32:37'),
(13, 'Super', 'super@teste.com', 'teste', '64fbe672e40522debb31a17595278f83c3b83bb6', 4, '2012-10-29 16:36:19'),
(14, 'Teste03', 'teste@teste.com.br', 'tese3', '64fbe672e40522debb31a17595278f83c3b83bb6', 4, '2012-10-29 16:36:57'),
(15, 'Teste 04', 'teste@teste.com', 'teste', '64fbe672e40522debb31a17595278f83c3b83bb6', 4, '2012-10-29 16:37:42'),
(16, 'Jefferson', 'internet@ondasete.com.br', 'admin', 'c98115cd358b91ecdeb15abd7fb0b3f8b95c2418', 1, '2012-11-05 10:11:25');
