<?php
App::uses('Farm', 'Model');

/**
 * Farm Test Case
 *
 */
class FarmTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.farm'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Farm = ClassRegistry::init('Farm');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Farm);

		parent::tearDown();
	}

}
