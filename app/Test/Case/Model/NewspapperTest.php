<?php
App::uses('Newspapper', 'Model');

/**
 * Newspapper Test Case
 *
 */
class NewspapperTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.newspapper'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Newspapper = ClassRegistry::init('Newspapper');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Newspapper);

		parent::tearDown();
	}

}
