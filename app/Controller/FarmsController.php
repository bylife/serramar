<?php
App::uses('AppController', 'Controller');
/**
 * Farms Controller
 *
 * @property Farm $Farm
 */
class FarmsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Farm->recursive = 0;
		$this->set('farms', $this->paginate());
	}

/**
* index method
* @return void
*/
	public function index(){
		$this->Farm->recursive = 0;
		$farms = $this->paginate();
		if($this->params['requested']){
			return $farms;
		} else {
			$this->set('farms', $farms);
			$this->set('page', 'fazendas');
			$subtitle = "Aqui você vai conhecer algumas das fazendas que representam o alto grau de excelência dos nossos produtores.";
			$this->set('subtitle', $subtitle);
	 	}
	}
	/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($slug = null) {
		$this->layout = 'default';
		if(!isset($slug)){
			$this->redirect('/');
		} else {
			if($farm = $this->Farm->findBySlug($slug)){
				$this->set('farm', $farm);
				$this->set('page', 'fazendas');
				$subtitle = "Aqui você vai conhecer algumas das fazendas que representam o alto grau de excelência dos nossos produtores.";
				$this->set('subtitle', $subtitle);
			} else {
				$this->Session->setFlash('Fazenda não cadastrada', 'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
		}
	}
/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Farm->id = $id;
		if (!$this->Farm->exists()) {
			throw new NotFoundException(__('Invalid %s', __('farm')));
		}
		$this->set('farm', $this->Farm->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Farm->create();
			if ($this->Farm->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('farm')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('farm')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Farm->id = $id;
		if (!$this->Farm->exists()) {
			throw new NotFoundException(__('Invalid %s', __('farm')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Farm->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('farm')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('farm')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Farm->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Farm->id = $id;
		if (!$this->Farm->exists()) {
			throw new NotFoundException(__('Invalid %s', __('farm')));
		}
		if ($this->Farm->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('farm')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('farm')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
