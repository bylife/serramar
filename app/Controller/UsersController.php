<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail','Network/Email');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {


/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('RequestHandler');

	public function beforeFilter() {
    	
    	/*$this->Auth->deny();
    	$this->Auth->allow('login', 'logout');*/
    	parent::beforeFilter();
	}

/**
 * login method
 *
 * @return void
 */
	public function login(){
		$this->layout = 'admin';
		if ($this->Session->read('Auth.User')) {
			$this->Session->setFlash('Você está logado!');
			$this->redirect('/', null, false);
		}

		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash('Nome de usuário ou senha inválidos.');
			}
		}
	}
/**
 * logout method
 *
 * @return void
 */
	public function logout(){
		$this->Session->setFlash('Good-Bye');
		$this->redirect($this->Auth->logout());
	}

/**
* painel de controle
*
* @return void
*/

	public function admin_painel(){
		$this->Session->setFlash('Bem-vindo!'. $this->Auth->name, 'alert', array('plugin' => 'TwitterBootstrap', 'class' => 'alert-success'));
	}
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid %s', __('user')));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('user')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('user')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid %s', __('user')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('user')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('user')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid %s', __('user')));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('user')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('user')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}

	public function varejistas(){
		$this->set('page', 'varejistas');
		$subtitle = "Encante os seus clientes! Ofereça a eles a alta qualidade dos produtos SERRAMAR. Solicite, pelo cadastro ao lado, a visita dos nossos representantes.";
		$this->set('subtitle', $subtitle);
	}


	public function contato(){
		$this->layout = 'ajax';		
		if($this->RequestHandler->isAjax()){			
			$email = new CakeEmail();		
			$email->from('serramar@ondasete.com.br')			
     			  ->config('smtp')
				  ->to('secretaria@serramar.coop.br')
				  ->cc('maringa@provale.com.br')
				  ->replyTo($this->request->data['User']['email'])
				  //->to('ricardo.erik@gmail.com')
			      ->subject('Contato Site - Serramar.coop.br')
			      ->template('email_contato')
			      ->emailFormat('html')
			      ->viewVars(array('data' => $this->request->data));
			
			if($email->send()){								
				$this->Session->setFlash('Mensagem enviada com sucesso!', 'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-sucess'));
				$this->render('contato');
			} else {
				$this->Session->setFlash('Não foi possível enviar a mensagem', 'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->render('contato');
			}
		}		
	}
/**
* Cadastro de Varejista
*
* @return void
*/


	

}
