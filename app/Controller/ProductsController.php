<?php
App::uses('AppController', 'Controller');
/**
 * Products Controller
 *
 * @property Product $Product
 */
class ProductsController extends AppController {

	public function beforeFilter() {
		$directory = $this->webroot . 'files' . DS . 'product' . DS . 'picture' . DS;
		$this->set('directory', $directory);
		$this->set('tabelanutricional', $this->Product->getTabelaNutricional());
		return parent::beforeFilter();
	}

/**
 *  Layout
 *
 * @var string
 */
	public $layout = 'bootstrap';

/**
 * 
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');

	public function index($line = null){
		$this->layout = 'default';
		$this->Product->recursive = 0;
		$this->redirect('/#produtos');
	}

	public function serramar(){
		$this->layout = 'default';
		$this->Product->recursive = 0;
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'serramar')));
		$this->set('products', $products);
		$this->set('page', 'produtos');
		$this->set('subtitle', 'Conheça a linha de produtos feitos com o mais delicioso leite das fazendas, um privilégio que você pode ter todos os dias em sua casa');
	}

	public function maringa(){
		$this->layout = 'default';
		$this->Product->recursive = 0;
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'maringa')));
		$this->set('products', $products);
		$this->set('page', 'produtos');
		$this->set('subtitle', 'A tradição que põe um sabor especial na sua mesa.');
	}

	public function milkmix(){
		$this->layout = 'default';
		$this->Product->recursive = 0;
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'milkmix')));
		$this->set('products', $products);
		$this->set('page', 'produtos');
		$this->set('subtitle', 'A saborosa bebida láctea da Serramar em novos e deliciosos sabores: ameixa e laranja. Saiba mais!');
	}

	public function serramar_all($id = null){
		$this->layout = 'ajax';
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'serramar')));
		$this->set('products', $products);
		$this->set('id', $id);
	}
	public function milkmix_all($id = null){
		$this->layout = 'ajax';
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'milkmix')));
		$this->set('products', $products);
		$this->set('id', $id);
		
	}
	public function maringa_all($id = null){
		$this->layout = 'ajax';
		$products = $this->Product->find('all', array('conditions'=>array('Product.line'=>'maringa')));
		$this->set('products', $products);
		$this->set('id', $id);
		
	}

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid %s', __('product')));
		}
		$this->set('product', $this->Product->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('product')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('product')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}				
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid %s', __('product')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('product')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('product')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Product->read(null, $id);
		}

	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid %s', __('product')));
		}
		if ($this->Product->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('product')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('product')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
