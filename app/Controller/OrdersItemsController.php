<?php
App::uses('AppController', 'Controller');
/**
 * OrdersItems Controller
 *
 * @property OrdersItem $OrdersItem
 */
class OrdersItemsController extends AppController {

/**
 *  Layout
 *
 * @var string
 */
	public $layout = 'bootstrap';

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->OrdersItem->recursive = 0;
		$this->set('ordersItems', $this->paginate());
	}

/**
 * admin_view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->OrdersItem->id = $id;
		if (!$this->OrdersItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('orders item')));
		}
		$this->set('ordersItem', $this->OrdersItem->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->OrdersItem->create();
			if ($this->OrdersItem->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('orders item')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('orders item')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$orders = $this->OrdersItem->Order->find('list');
		$products = $this->OrdersItem->Product->find('list');
		$this->set(compact('orders', 'products'));
	}

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->OrdersItem->id = $id;
		if (!$this->OrdersItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('orders item')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->OrdersItem->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('orders item')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('orders item')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->OrdersItem->read(null, $id);
		}
		$orders = $this->OrdersItem->Order->find('list');
		$products = $this->OrdersItem->Product->find('list');
		$this->set(compact('orders', 'products'));
	}

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->OrdersItem->id = $id;
		if (!$this->OrdersItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('orders item')));
		}
		if ($this->OrdersItem->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('orders item')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('orders item')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
