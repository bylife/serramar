<?php
App::uses('AppController', 'Controller');


class RetailersController extends AppController{

	/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */

public $components = array('RequestHandler');

public function cadastro_varejistas(){
		$this->layout = "ajax";
		$this->autoRender = false;
		if($this->request->is('ajax')){
			
			$this->request->data['Retailer']['name'] = $this->request->data['nome'];
			$this->request->data['Retailer']['email'] = $this->request->data['email'];
			$this->request->data['Retailer']['phone'] = $this->request->data['telefone'];
			$this->request->data['Retailer']['address'] = $this->request->data['endereco'];
			$this->request->data['Retailer']['city'] = $this->request->data['cidade'];
			
			//debug($this->request->data); die();
			
			$this->Retailer->create();
			if($this->Retailer->save($this->request->data)){
				return true;
			} else {
				return false;
			}
		}
	
	}
	

	public function admin_index() {
		$this->set('retailers', $this->paginate());
	}

	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Retailer->id = $id;
		if (!$this->Retailer->exists()) {
			throw new NotFoundException(__('Invalid %s', __('retailer')));
		}
		if ($this->Retailer->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('retailer')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('retailer')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}