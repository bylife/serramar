<?php
App::uses('AppController', 'Controller');
/**
 * Newspappers Controller
 *
 * @property Newspapper $Newspapper
 */
class NewspappersController extends AppController {

	public function beforeFilter(){
		$this->set('page', 'cooperador');
		$subtitle = "Leia as últimas edições do jornal O Cooperador.";
		$this->set('subtitle', $subtitle);

		return parent::beforeFilter();
	}

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Newspapper->recursive = 0;
		$this->set('newspappers', $this->paginate());
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Newspapper->recursive = 0;
		$newspappers =  $this->paginate();
		if($this->params['requested']){
			return $newspappers;
		}
		$this->set('newspappers', $newspappers);
		$this->set('last', $this->Newspapper->find('first', array('order' => array('Newspapper.created' => 'desc'))));
	}
/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Newspapper->id = $id;
		if (!$this->Newspapper->exists()) {
			throw new NotFoundException(__('Invalid %s', __('newspapper')));
		}
		$this->set('newspapper', $this->Newspapper->read(null, $id));
	}
/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Newspapper->id = $id;
		if (!$this->Newspapper->exists()) {
			throw new NotFoundException(__('Invalid %s', __('newspapper')));
		}
		$this->set('newspapper', $this->Newspapper->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Newspapper->create();
			if ($this->Newspapper->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('newspapper')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('newspapper')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Newspapper->id = $id;
		if (!$this->Newspapper->exists()) {
			throw new NotFoundException(__('Invalid %s', __('newspapper')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Newspapper->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('newspapper')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('newspapper')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Newspapper->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Newspapper->id = $id;
		if (!$this->Newspapper->exists()) {
			throw new NotFoundException(__('Invalid %s', __('newspapper')));
		}
		if ($this->Newspapper->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('newspapper')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('newspapper')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
}
