<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $helpers = array(
		'TinyMCE',
		'Html' => array('className'=> 'TwitterBootstrap.BootstrapHtml'),
		'Session',
		'Js',
		'Form' => array('className'=> 'TwitterBootstrap.BootstrapForm'),
		'Paginator' => array('className'=> 'TwitterBootstrap.BootstrapPaginator'),
		'Text'
	);

	public $components = array(
				'Acl',
				'Auth' => array(
						'authorize' => array('Actions' => array('actionPath' => 'controllers'))
					),
				'Session'
			);
	

	protected function _isPrefix($prefix) {
                return (isset($this->request->params['prefix']) && ($this->request->params['prefix'] === $prefix));
    }

    public function beforeFilter(){
		
		$this->layout = 'default';

     	$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin'=>false);
     	$this->Auth->logoutRedirect = array('controller' => '/', 'action' => 'index', 'admin'=>false);
     	$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'painel', 'admin'=> true);

     	
     	$this->Auth->deny();
        $this->Auth->allow('display','contato','login','logout', 'cadastro_varejistas');
     	//$this->Auth->allow();

            if(!$this->_isPrefix('admin')){
                    $this->Auth->allow();
            } else {
                    $this->layout = 'admin';
                    $this->set( 'loggedIn', $this->Auth->loggedIn() );
                    $group = $this->Auth->user('group_id');

                    switch($group){
                    	case 1:
                    		$this->set('menu', 'adminMenu');
                    		break;
                    	case 2:
                    		$this->set('menu', 'managerMenu');
                    		break;
                    	case 3:
                    		$this->set('menu', 'coopMenu');
                    		break;
                    }
            }

           return parent::beforeFilter();
    }
}
