<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Orders Item');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($ordersItem['OrdersItem']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Order'); ?></dt>
			<dd>
				<?php echo $this->Html->link($ordersItem['Order']['id'], array('controller' => 'orders', 'action' => 'view', $ordersItem['Order']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Product'); ?></dt>
			<dd>
				<?php echo $this->Html->link($ordersItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $ordersItem['Product']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Price'); ?></dt>
			<dd>
				<?php echo h($ordersItem['OrdersItem']['price']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Quantity'); ?></dt>
			<dd>
				<?php echo h($ordersItem['OrdersItem']['quantity']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Orders Item')), array('action' => 'edit', $ordersItem['OrdersItem']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Orders Item')), array('action' => 'delete', $ordersItem['OrdersItem']['id']), null, __('Are you sure you want to delete # %s?', $ordersItem['OrdersItem']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders Items')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Orders Item')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders')), array('controller' => 'orders', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('controller' => 'orders', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('controller' => 'products', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

