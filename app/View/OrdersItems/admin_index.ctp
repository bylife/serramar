<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __('List %s', __('Orders Items'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('order_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('product_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('price');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('quantity');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($ordersItems as $ordersItem): ?>
			<tr>
				<td><?php echo h($ordersItem['OrdersItem']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($ordersItem['Order']['id'], array('controller' => 'orders', 'action' => 'view', $ordersItem['Order']['id'])); ?>
				</td>
				<td>
					<?php echo $this->Html->link($ordersItem['Product']['name'], array('controller' => 'products', 'action' => 'view', $ordersItem['Product']['id'])); ?>
				</td>
				<td><?php echo h($ordersItem['OrdersItem']['price']); ?>&nbsp;</td>
				<td><?php echo h($ordersItem['OrdersItem']['quantity']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $ordersItem['OrdersItem']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ordersItem['OrdersItem']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ordersItem['OrdersItem']['id']), null, __('Are you sure you want to delete # %s?', $ordersItem['OrdersItem']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Orders Item')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders')), array('controller' => 'orders', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('controller' => 'orders', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('controller' => 'products', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>