<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Information');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($information['Information']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Product'); ?></dt>
			<dd>
				<?php echo $this->Html->link($information['Product']['name'], array('controller' => 'products', 'action' => 'view', $information['Product']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Item'); ?></dt>
			<dd>
				<?php echo h($information['Information']['item']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Reference Value'); ?></dt>
			<dd>
				<?php echo h($information['Information']['reference_value']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Value'); ?></dt>
			<dd>
				<?php echo h($information['Information']['value']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($information['Information']['created']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Update'); ?></dt>
			<dd>
				<?php echo h($information['Information']['update']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Information')), array('action' => 'edit', $information['Information']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Information')), array('action' => 'delete', $information['Information']['id']), null, __('Are you sure you want to delete # %s?', $information['Information']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Information')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Information')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('controller' => 'products', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

