<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __('List %s', __('Information'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('product_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('item');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('reference_value');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('value');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('created');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('update');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($information as $information): ?>
			<tr>
				<td><?php echo h($information['Information']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($information['Product']['name'], array('controller' => 'products', 'action' => 'view', $information['Product']['id'])); ?>
				</td>
				<td><?php echo h($information['Information']['item']); ?>&nbsp;</td>
				<td><?php echo h($information['Information']['reference_value']); ?>&nbsp;</td>
				<td><?php echo h($information['Information']['value']); ?>&nbsp;</td>
				<td><?php echo h($information['Information']['created']); ?>&nbsp;</td>
				<td><?php echo h($information['Information']['update']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $information['Information']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $information['Information']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $information['Information']['id']), null, __('Are you sure you want to delete # %s?', $information['Information']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Information')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('controller' => 'products', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>