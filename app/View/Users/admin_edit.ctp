<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('User', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend>Editar Usuário</legend>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'label' => 'Nome Completo',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('email', array(
					'required' => 'required',
					'label' => 'E-mail',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('username', array(
					'required' => 'required',
					'label' => 'Login',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('password', array(
					'label' => 'Senha',
					'value' => ''
					)
				);
				echo $this->BootstrapForm->input('group_id', array(
					'required' => 'required',
					'label' => 'Grupo',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->hidden('id');
				?>
				<?php echo $this->BootstrapForm->submit(__('Salvar'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header">Ações</li>
			<li><?php echo $this->Form->postLink('Excluir Usuário', array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
			<li><?php echo $this->Html->link('Usuários', array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link('Grupos', array('controller' => 'groups', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Novo Grupo', array('controller' => 'groups', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>