<div class="rows produtor area">
	<h3>Cadastro de Varejista</h3>
	<div class="container">
		<p>Cadastre-se</p>
		<p>
			Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI.
		</p>
		<div class="form-horizontal">
			<?php
				echo $this->Form->create('User');
			?>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'label' => 'Nome',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('email', array(
					'required' => 'required',
					'label' => 'E-mail',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('username', array(
					'required' => 'required',
					'label' => 'Login',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('password', array(
					'required' => 'required',
					'label' => 'Senha',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->hidden('id');
				?>
				<?php echo $this->BootstrapForm->submit(__('Cadastrar'));?>
				<?php echo $this->BootstrapForm->end();?>
			</form>
		</div>
	</div>
</div><!-- .rows.produtor -->