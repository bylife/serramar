<div class="row-fluid">
	<div class="span9">
		<h2>Usuários Cadastrados</h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('email');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('username');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('group_id');?></th>	
				<th class="actions">Ações</th>
			</tr>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
				<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
				</td>
				<td class="actions">
					<?php echo $this->Html->link(__('Visualizar'), array('action' => 'view', $user['User']['id'])); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id'])); ?>&nbsp;&nbsp;|&nbsp;&nbsp;
					<?php echo $this->Form->postLink(__('Apagar'), array('action' => 'delete', $user['User']['id']), null, __('Tem certeza que deseja excluir o usuário # %s?', $user['User']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header">Ações</li>
			<li><?php echo $this->Html->link(__('Cadastrar %s', __('Usuário')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Grupos'), array('controller' => 'groups', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('Cadastrar Grupo'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('Pedidos'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
		</ul>
		</div>
	</div>
</div>