<div class="rows">
			<div class="span12">
				<div class="usuario">
					<span class="h4">Usuário logado: </span>
					<span>Ricardo - Ondasete MKT. Com.</span>
					<button class="btn btn-orange">Sair</button>
				</div>
			</div>
		</div><!-- .rows > .usuario -->
		<div class="rows">
			<div class="span12">
				<div class="span6 catalogo">
					<h3>Catálogo de Produtos</h3>
					<p>
						Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI.
					</p>
				</div>
				<div class="span6 carrinho">
					<div class="rows">
						<h4>Carrinho</h4>
					</div>
					<div class="rows info">
						<span class="quant">03 itens ..................................................................................................................................</span>
						<span class="total">Total: R$ </span>
						<span class="valor">73,00</span>
					</div>
					<div class="rows">
						<button class="btn btn-orange">Finalizar pedido</button>
					</div>
				</div>
			</div>
		</div><!-- .rows > .catelogo + .carrinho -->
		<div class="rows">
			<div class="produto">
				<figure>
					<img src="img/produto_leite_serramar.jpg" alt="">
				</figure>
				<div class="info">
					<div class="rows">
						<h3>Leite UHT</h3>
						<h4>Serramar</h4>
					</div>
					<table>
						<tr>
							<td>Produto:</td>
							<td class="desc">Leite UHT Tipo B</td>
						</tr>
						<tr>
							<td>Cód. Interno:</td>
							<td class="desc">0123456789</td>
						</tr>
						<tr>
							<td>Acondicionamento:</td>
							<td class="desc">Cx. com 16 x 1L</td>
						</tr>
						<tr>
							<td>Temperatura:</td>
							<td class="desc">6 °C</td>
						</tr>
						<tr>
							<td>Validade:</td>
							<td class="desc">180 dias</td>
						</tr>
						<tr>
							<td>EAN 13:</td>
							<td class="desc">0123456798</td>
						</tr>
						<tr>
							<td>DUNI 14:</td>
							<td class="desc">0123456789</td>
						</tr>
					</table>
				</div><!-- .info -->
			</div><!-- .produto -->
			<div class="add">
				<div class="rows">
					<h3>Adicionar ao Carrinho</h3>
				</div>
				<div class="rows">
					<div class="span6 produto-add">
						<h5>Produto:</h5>
						<div class="field-group">
							<input type="text" class="field">
							<button class="btn dropdown-toggle">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#">Leite A</a></li>
								<li><a href="#">Leite B</a></li>
								<li><a href="#">Leite C</a></li>
							</ul>
						</div>
					</div>
					<div class="span6 produto-quant">
						<h5>Quant:</h5>
						<input type="text">
					</div>
					<div class="span4 enviar"><button class="btn btn-green">Adicionar</button></div>
				</div>
			</div><!-- .add -->
		</div><!-- .rows -->
		<div class="rows">
			<div class="produto">
				<figure>
					<img src="img/produto_leite_serramar.jpg" alt="">
				</figure>
				<div class="info">
					<div class="rows">
						<h3>Leite UHT</h3>
						<h4>Serramar</h4>
					</div>
					<table>
						<tr>
							<td>Produto:</td>
							<td class="desc">Leite UHT Tipo B</td>
						</tr>
						<tr>
							<td>Cód. Interno:</td>
							<td class="desc">0123456789</td>
						</tr>
						<tr>
							<td>Acondicionamento:</td>
							<td class="desc">Cx. com 16 x 1L</td>
						</tr>
						<tr>
							<td>Temperatura:</td>
							<td class="desc">6 °C</td>
						</tr>
						<tr>
							<td>Validade:</td>
							<td class="desc">180 dias</td>
						</tr>
						<tr>
							<td>EAN 13:</td>
							<td class="desc">0123456798</td>
						</tr>
						<tr>
							<td>DUNI 14:</td>
							<td class="desc">0123456789</td>
						</tr>
					</table>
				</div><!-- .info -->
			</div><!-- .produto -->
			<div class="add">
				<div class="rows">
					<h3>Adicionar ao Carrinho</h3>
				</div>
				<div class="rows">
					<div class="span6 produto-add">
						<h5>Produto:</h5>
						<div class="field-group">
							<input type="text" class="field">
							<button class="btn dropdown-toggle">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="#">Leite A</a></li>
								<li><a href="#">Leite B</a></li>
								<li><a href="#">Leite C</a></li>
							</ul>
						</div>
					</div>
					<div class="span6 produto-quant">
						<h5>Quant:</h5>
						<input type="text">
					</div>
					<div class="span4 enviar"><button class="btn btn-green">Adicionar</button></div>
				</div>
			</div><!-- .add -->
		</div><!-- .rows -->