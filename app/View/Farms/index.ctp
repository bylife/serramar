		<?php foreach($farms as $farm) :
			$img = $this->webroot .'files'.DS.'farm'.DS.'picture'.DS.$farm['Farm']['path_picture'].DS.'thumb_'.$farm['Farm']['picture'];
		?>
		<div class="rows">
			<a href="<?php echo $this->Html->url('/fazendas/').$farm['Farm']['slug'];?>" class="cols figure">
				<img src="<?=$img;?>" alt="">
			</a>
			<div class="cols">
				<h2>
					<?php echo $farm['Farm']['title'];?>
				</h2>
				<?php echo $this->Text->truncate($farm['Farm']['body'], 180 ,array('ending'=>'...','extact'=>true));?>
			</div>
		</div>
		<?php endforeach;?>
		