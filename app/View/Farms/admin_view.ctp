<div class="row-fluid">
	<div class="span9">
		<h2>Fazenda Serramar</h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($farm['Farm']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Title'); ?></dt>
			<dd>
				<?php echo h($farm['Farm']['title']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Body'); ?></dt>
			<dd>
				<?php echo $farm['Farm']['body']; ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Picture'); ?></dt>
			<dd>
				<?php $img = $this->webroot."files".DS.'farm'.DS."picture".DS.$farm['Farm']['path_picture'].DS.'thumb_'.$farm['Farm']['picture'];; ?>
				<img src="<?php echo $img;?>" alt="">
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo 'Ações'; ?></li>
			<li><?php echo $this->Html->link('Editar Fazenda', array('action' => 'edit', $farm['Farm']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink('Excluir Fazenda', array('action' => 'delete', $farm['Farm']['id']), null, __('Tem certeza que deseja excluir esta Fazenda')); ?> </li>
			<li><?php echo $this->Html->link('Fazendas Serramar', array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link('Nova Fazenda Serramar', array('action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

