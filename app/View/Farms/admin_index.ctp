<div class="row-fluid">
	<div class="span9">
		<h2>Fazendas Serramar</h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>
		
		<ul class="thumbnails">
		<?php foreach ($farms as $farm): ?>
			<li class="span3">
				<div class="thumbnail">
					<?php $img = $this->webroot."files".DS.'farm'.DS."picture".DS.$farm['Farm']['path_picture'].DS.'thumb_'.$farm['Farm']['picture']; ?>
					<img src="<?php echo $img;?>" alt="">
					<h3><?php echo h($farm['Farm']['title']); ?></h3>
					<p><?php echo $this->Text->truncate($farm['Farm']['body'], 180, array('ending'=>'', 'exact'=> '')); ?></p>
					<p>
					<?php echo $this->Html->link('Visualizar', array('action' => 'view', $farm['Farm']['id']),array('class'=>'btn btn-mini')); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $farm['Farm']['id']), array('class'=>'btn btn-mini')); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $farm['Farm']['id']), array('class'=>'btn btn-mini'), __('Are you sure you want to delete # %s?', $farm['Farm']['id'])); ?>
					</p>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo 'Ações'; ?></li>
			<li><?php echo $this->Html->link('Nova Fazenda Serramar', array('action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>