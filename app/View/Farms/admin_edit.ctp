<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Farm', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend>Cadastrar Fazenda Serramar</legend>
				<?php
				echo $this->BootstrapForm->input('title', array(
					'required' => 'required',
					'label' => 'Título',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->TinyMCE->input('Farm.body', array('label'=>'Texto'), array('language'=> 'en', 'width'=> '660', 'height'=>'600'), 'full');
				echo $this->BootstrapForm->input('picture', array(
					'type'=>'file',
					'label' => 'Imagem destacada',
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('path_picture', array('type'=>'hidden'));
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo 'Ações'; ?></li>
			<li><?php echo $this->Form->postLink('Excluir esta fazenda', array('action' => 'delete', $this->Form->value('Farm.id')), null, __('Tem certeza que deseja excluir # %s?', $this->Form->value('Farm.id'))); ?></li>
			<li><?php echo $this->Html->link('Fazenda', array('action' => 'index'));?></li>
		</ul>
		</div>
	</div>
</div>