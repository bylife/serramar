<div id="main-menu">
	<ul class="nav nav-pills">
		<li><?php echo $this->Html->link('A Serramar',array('controller'=>'/','action'=>'index', '#'=>'historia'));?></li>
		<li><?php echo $this->Html->link('Produtos',array('controller'=>'/','action'=>'index', '#'=>'produtos'));?></li>
		<li><?php echo $this->Html->link('Cooperativa',array('controller'=>'/','action'=>'index', '#'=>'cooperativa'));?></li>
		<li class="logo">
			<a href="<?php echo $this->Html->url('/');?>"><img src="<?php echo $this->webroot;?>img/logo_serramar.png" alt="Serramar"></a>
		</li>
		<li><a href="<?php echo $this->Html->url('/blog/');?>">Blog</a></li>
		<li><?php echo $this->Html->link('Varejistas',array('controller'=>'/','action'=>'index', '#'=>'varejistas'));?></li>
		<li><?php echo $this->Html->link('Fale conosco',array('controller'=>'/','action'=>'index', '#'=>'contato'));?></li>
	</ul>
</div><!-- #main-menu -->