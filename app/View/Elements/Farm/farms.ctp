<?php $farms = $this->requestAction('/farms/index/sort:created/direction:asc/limit:2');?>
<ul class="posts">
	<?php if(!$farms): ?>
	<li class="rows dark">Nenhuma fazenda cadastrada</li>
	<?php else :
		$x = 1;
		foreach($farms as $farm):
			$img = $this->webroot .'files'.DS.'farm'.DS.'picture'.DS.$farm['Farm']['path_picture'].DS.'thumb_'.$farm['Farm']['picture'];
			$cor = !($x % 2) ? "light" : "dark";
	?>
	<li class="rows <?php echo $cor;?>">
		<a href="<?php echo $this->Html->url('/fazendas/').$farm['Farm']['slug'];?>" class="span1 figure"><img src="<?php echo $img;?>" alt=""></a>
		<aside class="span2">
			<?php echo $this->Text->truncate($farm['Farm']['body'], 130, array('ending'=>'', 'exact'=>false));?>
			<a class="btn-leia" href="<?php echo $this->Html->url('/fazendas/').$farm['Farm']['slug'];?>"></a>
		</aside>
	</li>
	<?php
		$x++;
		endforeach;
	endif;
	?>
</ul>
