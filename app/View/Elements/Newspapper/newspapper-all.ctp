<?php 
$newspappers = $this->requestAction('/newspappers/index/sort:created/direction:desc');
?>
<div id="carousel-jornal" class="carousel slide">
	<a class="seta prev blue" data-slide="prev" href="#carousel-jornal"></a>
	<a class="seta next blue" data-slide="next" href="#carousel-jornal"></a>
	<div class="carousel-inner">
<?php
$count = 1;
$col = 4;
echo '<div class="item active"><ul class="clear">';
	foreach ($newspappers as $key => $array):
		foreach ($array as $news){
			$img = $this->webroot.'files'.DS.'newspapper'.DS.'picture'.DS.$news['picture_path'].DS.'thumb_'.$news['picture'];
			echo '<li>';
			echo '
				<a href="' . $this->Html->url('/o-cooperador/') . $news['id'] . '">
					<img src="' . $img . '" alt="' . $news['title'] . '" />
				</a>
				<p>' . $news['title'] . '</p>
			';
			echo '</li>';
		}
		if($count % $col == 0){
			echo "</ul></div>";

			if(isset($newspappers[$key + 1]))
				echo '<div class="item"><ul class="clear">';
		}
		$count++;
	endforeach;
	if(count($newspappers) % $col != 0)
		echo '</ul></div>';
?>
	</div>
</div>
