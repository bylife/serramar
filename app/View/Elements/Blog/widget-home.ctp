<?php
	function limitarTexto($texto, $limite) {
		if(strlen($texto) >= $limite)
		  $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
		if( $texto == '...' || strlen($texto) <= 3)
			return false;
		else
			return $texto;
	}

	function serramar_color_category($category) {
		$color_default = 'silver';

		switch ($category) :
			case 'sabor_saude' :
				$color_category = 'blue';
				break;
			case 'mae_filho' :
				$color_category = 'green';
				break;
			case 'receitas' :
				$color_category = 'pink';
				break;
			case 'noticias' :
				$color_category = 'orange';
				break;
			case 'freelac' :
				$color_category = 'orange2';
				break;
			case 'light' :
				$color_category = 'yellow';
				break;
			default :
				$color_category = $color_default;
		endswitch;

		return $color_category;
	}
?>
	<header>
		<h3>Blog</h3>
	</header>
	<div id="blog-carousel" class="carousel slide">
		<a class="seta prev black" data-slide="prev" href="#blog-carousel"></a>
		<a class="seta next black" data-slide="next" href="#blog-carousel"></a>
		<div class="carousel-inner">
		<?php
		$k = 0;
		$cols = 4;		
		$posts = $wp_posts['Blog'];
		$count = count($posts);
			for($i=0;$i <= $count-1; $i++){
				$cat = serramar_color_category($posts[$i]['cat_slug']);
				if($i==0) { echo '<div class="item active">';}
				?>
				<article class="<?=$cat;?>">
					<h2><?=$this->Text->truncate($posts[$i]['title'], 20, array('ending' => '', 'exact' => true)); ?></h2>
					<figure>
						<a href="<?= $posts[$i]['link']; ?>">
							<img src="<?php echo $posts[$i]['site_url'].'/wp-content/uploads/'.$posts[$i]['thumbnail'];?>" alt="<?=$posts[$i]['title'];?>" width="217px" height="130px">
						</a>
					</figure>
					<div class="text">
						<p>
						<?php echo $this->Text->truncate($posts[$i]['content'],130, array('ending'=>'...', 'exact'=>true));?>
						</p>
						<a class="leia" href="<?= $posts[$i]['link']; ?>"></a>
					</div>
				</article>
			<?php					
					if((($i+1) % $cols) == 0){
						echo '</div>';
						if(isset($posts[$i + 1])){
							echo '<div class="item">';
						}
					}
			}							
	?>
</div>
