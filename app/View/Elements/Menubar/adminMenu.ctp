<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="i-bar"></span>
				</a>
				<a class="brand" href="<?=$this->Html->url('/admin');?>">Serramar - Painel de controle</a>
				<div class="btn-group pull-right">
					<a class="btn btn-primary" href="#"><i class="icon-user icon-white"></i> <?php echo AuthComponent::user('name');?></a>
  					<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>      
	                       
		            <ul class="dropdown-menu">      
		              <li><a href="#">Perfil</a></li>
		              <li class="divider"></li>
		              <li><?php echo $this->Html->link('Sair', array('controller'=>'users', 'action'=>'logout', 'admin'=> false));?></li>
		            </ul>
	          </div>

	          
				<div class="nav-collapse">
					<ul class="nav">
						<li><?=$this->Html->link('Grupos', array('controller'=>'groups', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Usuários', array('controller'=>'users', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Produtos', array('controller'=>'products', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Fazendas Serramar', array('controller'=>'farms', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('O Cooperador', array('controller'=>'newspappers', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Pedidos', array('controller'=>'orders', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Varejistas', array('controller'=>'retailers', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Blog', '/blog/wp-admin');?></li>
						
					</ul>
				</div><!--/.nav-collapse -->
			
			</div>
		</div>
	</div>