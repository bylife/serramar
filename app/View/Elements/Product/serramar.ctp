<?php
$products = $this->requestAction('/products/serramar');
$c = 0;
foreach ($products as $p) :
	$img = $this->webroot.'files'.DS.'product'.DS.'picture'.DS.$p['Product']['path'].DS.$p['Product']['picture'];
	if($c == 0){
		$ativo = "active";
		$c++;
	} else {
		$ativo = "";
	}
?>
<div class="item <?php echo $ativo;?>">
	<a class="figure modal-remote" role="button" href="<?php echo $this->Html->url('/products/serramar_all/').$p['Product']['id'];?>" data-target="#modal-serramar" data-toggle="modal">
		<img src="<?php echo $img;?>" alt="<?php echo $p['Product']['name'];?>">
	</a>
	<p>
		<?php echo $p['Product']['name'] .' - '. $p['Product']['unit'];?>
	</p>
</div>
<?php
endforeach;
?>