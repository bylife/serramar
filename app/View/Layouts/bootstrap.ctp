<?php $this->loadHelper('TwitterBootstrap.BootstrapHtml'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Serramar Latcínios: 
		<?php echo $title_for_layout; ?>
	</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-responsive.min');
		echo $this->Html->script('bootstrap.min');


	?>
	
	<style>
		body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
		section {
			padding-top: 60px;
		}
	</style>

	<!-- Le fav and touch icons -->
	<!--
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	-->


</head>
<body>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="i-bar"></span>
				</a>
				<a class="brand" href="<?=$this->Html->url('/admin');?>">Serramar - Painel de controle</a>
				<?php if(isset($loggedIn)): ?>
				<div class="btn-group pull-right">            
	            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
	              <i class="icon-user"></i> <?php echo AuthComponent::user('name');?>
	              <span class="caret"></span>
	            </a>  
	                       
	            <ul class="dropdown-menu">
	           
	              <li><a href="#">Perfil</a></li>
	              <li class="divider"></li>
	              <li><?php echo $this->Html->link('Sair', array('controller'=>'users', 'action'=>'logout', 'admin'=> false));?></li>
	            </ul>
	            <?php endif;?>
	          </div>
	          <?php if(isset($loggedIn)): ?>
				<div class="nav-collapse">
					<ul class="nav">
						<li><?=$this->Html->link('Grupos', array('controller'=>'groups', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Usuários', array('controller'=>'users', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Produtos', array('controller'=>'products', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Fazendas Serramar', array('controller'=>'farms', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('O Cooperador', array('controller'=>'newspappers', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Pedidos', array('controller'=>'orders', 'action'=>'index', 'admin'=>true));?></li>
						<li><?=$this->Html->link('Blog', 'http://blog.serramar.coop.br/wp-admin');?></li>
						
					</ul>
				</div><!--/.nav-collapse -->
			<?php endif;?>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div> <!-- /container -->

	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	
	<?php echo $this->BootstrapHtml->script('bootstrap'); ?>
	<?php echo $this->fetch('script'); ?>
</body>
</body>
</html>