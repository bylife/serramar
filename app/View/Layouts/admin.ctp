<?php $this->loadHelper('TwitterBootstrap.BootstrapHtml'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		Serramar Latcínios: 
		<?php echo $title_for_layout; ?>
	</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-responsive.min');
		echo $this->Html->script('bootstrap.min');


	?>
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	<style>
		body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
		section {
			padding-top: 60px;
		}
	</style>

	<!-- Le fav and touch icons -->
	<!--
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	-->
<script type="text/javascript">

	function openKCFinder(c,a,b,d) {
    tinyMCE.activeEditor.windowManager.open({
        file: '<?=$this->webroot;?>js/kcfinder/browse.php?opener=tinymce&type='+b+'&dir='+b,
        title: 'KCFinder',
        width: 700,
        height: 500,
        resizable: "yes",
        inline: true,
        close_previous: "no",
        popup_css: false
    }, {
        window: d,
        input: c
    });
    return false;
}
	</script>

</head>
<body>
	<?php if(isset($loggedIn)): ?>
		<?php echo $this->Element('Menubar/'.$menu);?>
	<?php endif;?>

	<div class="container-fluid">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div> <!-- /container -->

	<!-- Le javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	
	<?php echo $this->BootstrapHtml->script('bootstrap'); ?>
	<?php echo $this->BootstrapHtml->script('lib/bootstrap-dropdown'); ?>
	<?php echo $this->BootstrapHtml->script('lib/bootstrap-transition');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-alert');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-modal');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-dropdown');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-scrollspy');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-tab');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-tooltip');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-popover');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-button');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-collapse');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-carousel');?>
    <?php echo $this->BootstrapHtml->script('lib/bootstrap-typeahead');?>
	<?php echo $this->fetch('script'); ?>
</body>
</body>
</html>