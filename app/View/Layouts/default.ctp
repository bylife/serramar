<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Serramar Laticínios</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css($this->Html->url('/less/bootstrap.less', true), 'stylesheet/less') . PHP_EOL;
		echo $this->Html->css($this->Html->url('/css/style.less', true), 'stylesheet/less') . PHP_EOL;

		echo $this->Html->script('libs/jquery');
		echo $this->Html->script('libs/less');
		echo $this->Html->script('jquery.preloadimages.js');
		echo $this->Html->script('libs/jquery.parallax-1.1.3');
		echo $this->Html->script('libs/jquery.localscroll-1.2.7-min');
		echo $this->Html->script('libs/scrolldeck/jquery.scrollTo-1.4.2-min');
		echo $this->Html->script('libs/scrolldeck/jquery.easing.1.3');
		echo $this->Html->script('bootstrap-tab');
		echo $this->Html->script('bootstrap-modal');
		echo $this->Html->script('bootstrap-carousel');
		echo $this->Html->script('bootstrap-transition');

		echo $this->Html->script('functions');
		echo $this->Html->script($this->Html->url('js/mask.js'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="<?php echo $page; ?> interna">
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<header class="corpo">
		<div class="wrap parallax-viewport">
		<?php echo $this->Element('main-menu');?>
			<div class="container">
				<div class="social right">
					<a class="facebook" href="http://www.facebook.com/serramarlaticinios" alt="Acesse nossa Fanpage no Facebook"></a>
					<a class="youtube" href="http://www.youtube.com/user/serramarlaticinios/videos" alt="Acesse nosso canal no Youtube"></a>
				</div><!-- .social -->
				<div class="sub-title">
					<?php echo $subtitle; ?>
				</div>
			</div><!-- .container -->
		</div>
		<div class="animation">
			<img src="<?php echo $this->webroot;?>img/bg_leite.png" alt="">
		</div>
	</header>

	<div class="container">

		<?php echo $this->fetch('content'); ?>

	</div>

	<footer>
		<div class="container">
			<div class="rows">
				<div class="span8">
					<a href="<?=$this->Html->url('/#historia');?>">Blog</a>
					<a href="<?=$this->Html->url('/blog/');?>">Blog</a>
					<a href="<?=$this->Html->url('/#cooperativa');?>">Cooperativa</a>
					<a href="<?=$this->Html->url('/#varejistas');?>">Varejistas</a>
				</div>
				<div class="span4">
					<div class="social">
						<a href="http://www.facebook.com/serramarlaticinios"><i class="facebook"></i></a>
						<a href="http://www.youtube.com/channel/UCdUSjNjc9LHh9u2uVMHwbng"><i class="youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</footer>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-47764450-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

</body>
</html>
