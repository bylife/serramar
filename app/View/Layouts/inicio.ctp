<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Serramar Laticínios</title>
	<?php
		echo $this->Html->meta('icon') . PHP_EOL;

		echo $this->Html->css($this->Html->url('/less/bootstrap.less', true), 'stylesheet/less') . PHP_EOL;
		echo $this->Html->css($this->Html->url('/css/style.less', true), 'stylesheet/less') . PHP_EOL;

		echo $this->Html->script('libs/jquery') . PHP_EOL;
		echo $this->Html->script('libs/less') . PHP_EOL;
		echo $this->Html->script('jquery.preloadimages.js') . PHP_EOL;
		echo $this->Html->script('libs/jquery.parallax-1.1.3') . PHP_EOL;
		echo $this->Html->script('libs/jquery.localscroll-1.2.7-min') . PHP_EOL;
		echo $this->Html->script('libs/scrolldeck/jquery.scrollTo-1.4.2-min') . PHP_EOL;
		echo $this->Html->script('libs/scrolldeck/jquery.easing.1.3') . PHP_EOL;
		echo $this->Html->script('bootstrap-tab') . PHP_EOL;
		echo $this->Html->script('bootstrap-modal') . PHP_EOL;
		echo $this->Html->script('bootstrap-carousel') . PHP_EOL;
		echo $this->Html->script('bootstrap-transition') . PHP_EOL;
		echo $this->Html->script('jquery.cycle.all') . PHP_EOL;

		echo $this->Html->script('functions') . PHP_EOL;

		echo $this->Html->script('mask.js');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body class="home">

	<div id="fb-root"></div>

	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<div class="loading">
		Carregando...
	</div>
	
	<div id="all">

		<ul id="menu-nav">
			<li><span>Sabor e Saúde Todo Dia</span><a href="#main-menu"></a></li>
			<li><span>A Serramar</span><a href="#historia"></a></li>
			<li><span>Produtos</span><a href="#produtos"></a></li>
			<li><span>Cooperativa</span><a href="#cooperativa"></a></li>
			<li><span>Varejistas</span><a href="#varejistas"></a></li>
			<li><span>Contato</span><a href="#contato"></a></li>
		</ul>

		<header class="corpo">
			<a href="http://www.serramar.coop.br/lancamento">
				<div id="mbox" style="background-image: url('img/slide_home1.jpg');"></div>
			</a>
			<div class="wrap parallax-viewport">
				<?php echo $this->Element('main-menu');?>
				<div class="container">
					<div class="social">
						<a class="facebook" href="http://www.facebook.com/serramarlaticinios" alt="Acesse nossa Fanpage no Facebook"></a>
					<a class="youtube" href="http://www.youtube.com/user/serramarlaticinios/videos" alt="Acesse nosso canal no Youtube"></a>
					</div><!-- .social -->
				</div>
			</div>
		</header>

		<?php echo $this->fetch('content'); ?>

	</div>
	<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-47764450-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
	
</body>
</html>
