<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>Serramar Laticínios</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css($this->Html->url('/less/bootstrap.less', true), 'stylesheet/less') . PHP_EOL;
		echo $this->Html->css($this->Html->url('/css/style.less', true), 'stylesheet/less') . PHP_EOL;

		echo $this->Html->script('libs/jquery');
		echo $this->Html->script('libs/less');
		echo $this->Html->script('jquery.preloadimages.js');
		echo $this->Html->script('libs/jquery.parallax-1.1.3');
		echo $this->Html->script('libs/jquery.localscroll-1.2.7-min');
		echo $this->Html->script('libs/scrolldeck/jquery.scrollTo-1.4.2-min');
		echo $this->Html->script('libs/scrolldeck/jquery.easing.1.3');
		echo $this->Html->script('bootstrap-tab');
		echo $this->Html->script('bootstrap-modal');
		echo $this->Html->script('bootstrap-carousel');
		echo $this->Html->script('bootstrap-transition');

		echo $this->Html->script('functions');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <meta property="og:title" content="Serramar Laticínios - Novos Leites" />
    <meta property="og:url" content="http://www.serramar.coop.br/lancamento" />
    <meta property="og:description" content="Viva os novos leites da família Serramar!" />
	<meta property="og:image" content="http://www.serramar.coop.br/img/hotsite_leites.jpg" />			
</head>
<body class="lancamentos">
		<script>
		(function(d){
		var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/es_LA/all.js";
		d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
	</script>

	<header class="corpo">
		<div class="wrap parallax-viewport">
			<div class="container">
				<div class="social left">
					<a href="http://www.facebook.com/serramarlaticinios" alt="Acesse nossa Fanpage no Facebook"><img src="<?php echo $this->webroot;?>img/icon-face.png" alt="Novos leites da Serramar"></figure></a>
					<a href="http://www.youtube.com/user/serramarlaticinios/videos" alt="Acesse nosso canal no Youtube"><img src="<?php echo $this->webroot;?>img/icon-yt.png" alt="Novos leites da Serramar"></figure></a>
				</div><!-- .social -->
				<div class="logo">
					<a href="<?php echo $this->Html->url('/');?>"><img src="<?php echo $this->webroot;?>img/logo-hotsite.png" alt="Serramar"></a>
				</div>
			</div><!-- .container -->
		</div>
		<div class="row-fluid">
			<div class="apresentacao span12">
				<figure><img src="<?php echo $this->webroot;?>img/apresentacao.png" alt="Novos leites da Serramar"></figure>
			</div>
		</div>
	</header>

	<div class="container">

		<?php echo $this->fetch('content'); ?>

	</div>

	<footer>
		<div class="container">
			<div class="span2">
				<div class="social pull-left">
					<a href="http://www.facebook.com/serramarlaticinios" alt="Acesse nossa Fanpage no Facebook"><img src="<?php echo $this->webroot;?>img/icon-face.png" alt="Novos leites da Serramar"></figure></a>
					<a href="http://www.youtube.com/user/serramarlaticinios/videos" alt="Acesse nosso canal no Youtube"><img src="<?php echo $this->webroot;?>img/icon-yt.png" alt="Novos leites da Serramar"></figure></a>
				</div>
			</div>
			<div class="address">
				<p>Avenida da Exposição, 353 - Vila Regina - Cx. Postal 23 - Guaratinguetá/SP CEP: 12522-190 SAC: 0800-7717116</p>
			</div>
		</div>
	</footer>
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-47764450-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

</body>
</html>
