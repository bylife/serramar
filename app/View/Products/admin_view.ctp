<div class="row-fluid">
	<div class="span9">
		<h2>Produto</h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($product['Product']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($product['Product']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Code'); ?></dt>
			<dd>
				<?php echo h($product['Product']['code']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Storage'); ?></dt>
			<dd>
				<?php echo h($product['Product']['storage']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Temperature'); ?></dt>
			<dd>
				<?php echo h($product['Product']['temperature']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Expiration'); ?></dt>
			<dd>
				<?php echo h($product['Product']['expiration']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Price'); ?></dt>
			<dd>
				<?php echo h($product['Product']['price']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Picture'); ?></dt>
			<dd>
				<?php echo h($product['Product']['picture']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Path'); ?></dt>
			<dd>
				<?php echo h($product['Product']['path']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Unit'); ?></dt>
			<dd>
				<?php echo h($product['Product']['unit']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Package'); ?></dt>
			<dd>
				<?php echo h($product['Product']['package']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Portions'); ?></dt>
			<dd>
				<?php echo h($product['Product']['portions']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Line'); ?></dt>
			<dd>
				<?php echo h($product['Product']['line']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($product['Product']['created']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Updated'); ?></dt>
			<dd>
				<?php echo h($product['Product']['updated']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Product')), array('action' => 'edit', $product['Product']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Product')), array('action' => 'delete', $product['Product']['id']), null, __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Information')), array('controller' => 'information', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Information')), array('controller' => 'information', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders')), array('controller' => 'orders', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Information')); ?></h3>
	<?php if (!empty($product['Information'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Product Id'); ?></th>
				<th><?php echo __('Item'); ?></th>
				<th><?php echo __('Reference Value'); ?></th>
				<th><?php echo __('Value'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th><?php echo __('Update'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($product['Information'] as $information): ?>
			<tr>
				<td><?php echo $information['id'];?></td>
				<td><?php echo $information['product_id'];?></td>
				<td><?php echo $information['item'];?></td>
				<td><?php echo $information['reference_value'];?></td>
				<td><?php echo $information['value'];?></td>
				<td><?php echo $information['created'];?></td>
				<td><?php echo $information['update'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'information', 'action' => 'view', $information['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'information', 'action' => 'edit', $information['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'information', 'action' => 'delete', $information['id']), null, __('Are you sure you want to delete # %s?', $information['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Information')), array('controller' => 'information', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Orders')); ?></h3>
	<?php if (!empty($product['Order'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('User Id'); ?></th>
				<th><?php echo __('Total'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($product['Order'] as $order): ?>
			<tr>
				<td><?php echo $order['id'];?></td>
				<td><?php echo $order['user_id'];?></td>
				<td><?php echo $order['total'];?></td>
				<td><?php echo $order['created'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'orders', 'action' => 'view', $order['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'orders', 'action' => 'edit', $order['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'orders', 'action' => 'delete', $order['id']), null, __('Are you sure you want to delete # %s?', $order['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('controller' => 'orders', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
