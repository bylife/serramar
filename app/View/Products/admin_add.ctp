<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Product', array( 'type'=>'file', 'class' => 'form-horizontal'));?>
			<fieldset>
				<legend>Cadastrar Produtos</legend>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'label' => 'Nome do Produto',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('line', array(
					'required' => 'required',
					'label' => 'Linha',
					'type' => 'select',
					'options' =>  array('serramar'=>'Serramar', 'maringa'=>'Maringá', 'milkmix'=>'MilkMix'),
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('picture', array(
					'required' => 'required',
					'label' => 'Foto',
					'type' => 'file',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('path', array('type' => 'hidden'));

				echo $this->BootstrapForm->input('unit', array(
					'required' => 'required',
					'label' => 'Unidade',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->TinyMCE->input('Product.nutrition', array('label'=>'Tabela Nutricional','value' => $tabelanutricional), array('language'=> 'en', 'width'=> '660', 'height'=>'600'), 'full');				
				
				?>
				<?php echo $this->BootstrapForm->submit('Cadastrar');?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header">Ações</li>
			<li><?php echo $this->Html->link('Produtos', array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link('Pedidos', array('controller' => 'orders', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Novo Pedido', array('controller' => 'orders', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>