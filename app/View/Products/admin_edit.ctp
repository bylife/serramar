<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Product', array( 'type'=>'file', 'class' => 'form-horizontal'));?>
			<fieldset>
				<legend>Cadastrar Produtos</legend>
				<?php
				echo $this->BootstrapForm->input('name', array(
					'required' => 'required',
					'label' => 'Nome do Produto',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('line', array(
					'required' => 'required',
					'label' => 'Linha',
					'type' => 'select',
					'options' =>  array('serramar'=>'Serramar', 'maringa'=>'Maringá', 'milkmix'=>'MilkMix'),
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);				
				echo $this->BootstrapForm->input('picture', array(					
					'label' => 'Foto',
					'type' => 'file',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('path', array('type' => 'hidden'));

				echo $this->BootstrapForm->input('unit', array(
					'required' => 'required',
					'label' => 'Unidade',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);				
				$value_nutririon = (!empty($this->request->data['Product']['nutrition'])) ? $this->request->data['Product']['nutrition'] : $tabelanutricional;
				echo $this->TinyMCE->input('Product.nutrition', array('label'=>'Tabela Nutricional','value' => $value_nutririon), array('language'=> 'en', 'width'=> '660', 'height'=>'600'), 'full');
				
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Product.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Product.id'))); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Information')), array('controller' => 'information', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Information')), array('controller' => 'information', 'action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders')), array('controller' => 'orders', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('controller' => 'orders', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>