<div class="rows">
	<div class="span12 catalogo">
		<h3>Produtos Serramar</h3>
		<p>
			O mais delicioso leite pasteurizado, direto das fazendas do interior do estado de são Paulo para sua casa,  produzindo uma linha irresistível de produtos.
		</p>
	</div>
</div><!-- .rows > .catelogo + .carrinho -->

<div class="rows">
	<?php foreach ($products as $product) : ?>
	<div class="produto produto-box">
		<a href="#modal-<?= $product['Product']['id']; ?>" class="title" role="button" data-toggle="modal">
			<?= $product['Product']['name']; ?>
		</a>
		<a href="#modal-<?= $product['Product']['id']; ?>" class="" role="button" data-toggle="modal">
			<figure>
				<img src="<?= $directory . $product['Product']['path'] . DS . $product['Product']['picture']; ?>" alt="">
			</figure>
		</a>
	</div><!-- .produto -->
	<?php endforeach; ?>
	<span class="clearfix"></span>
</div><!-- .rows -->

<!-- Modal dos produtos -->
<?php foreach ($products as $product) : ?>
<div id="modal-<?= $product['Product']['id']; ?>" class="modal-product modal hide fade" tabindex="-1" role="dialog" aria-labelledby="label-modal-serramar" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> × </button>
		<h3 id="label-modal-serramar">
			<?= $product['Product']['name']; ?>
		</h3>
	</div><!-- .modal-header -->
	<div class="modal-body">
		<div class = "modal-img">
			<figure>
				<img src="<?= $directory . $product['Product']['path'] . DS . $product['Product']['picture']; ?>" alt="">
			</figure>
		</div>
		<div class = "modal-table">
		<table class="table table-bordered">
			
			<tr>
				<td><?= $product['Product']['nutrition']; ?></td>
			</tr>
			<!-- <tr>
				<td>Temperatura</td>
				<td><?= $product['Product']['temperature']; ?></td>
			</tr>
			<tr>
				<td>Validade</td>
				<td><?= $product['Product']['expiration']; ?></td>
			</tr> -->
		</table>
			</div>
	</div><!-- .modal-body -->
</div><!-- #modal-serramar -->
<?php endforeach; ?>