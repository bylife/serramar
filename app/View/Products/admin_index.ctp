<div class="row-fluid">
	<div class="span9">
		<h2>Produtos</h2>

		
		<ul class="thumbnails">
		<?php foreach ($products as $product): ?>
			<li class="span3">
				<div class="thumbnail">
					<img src="<?php echo $this->webroot.'files'.DS.'product'.DS.'picture'.DS.$product['Product']['path'].DS.$product['Product']['picture'];?>" alt="<?php echo $product['Product']['name'];?>" class="img-polaroid" />
					<h3><?php echo h($product['Product']['name']); ?></h3>					
					<p>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $product['Product']['id']), array('class'=>'btn btn-mini')); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class'=>'btn btn-mini')); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $product['Product']['id']), array('class'=>'btn btn-mini'), __('Tem certeza que deseja excluir este produto?')); ?>
					</p>
				</div>
			</li>
		<?php endforeach; ?>
		</ul>
		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>
		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Novo Produto'), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('Pedidos'), array('controller' => 'orders', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('Novo Pedido'), array('controller' => 'orders', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>