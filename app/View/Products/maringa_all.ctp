	<div id="modal-carousel-serramar" class="carousel slide">
		<a class="carousel-control left" href="#modal-carousel-serramar" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#modal-carousel-serramar" data-slide="next">&rsaquo;</a>
		<div class="carousel-inner">
			<?php
				foreach ($products as $key => $product):
				$img = $this->webroot.'files'.DS.'product'.DS.'picture'.DS.$product['Product']['path'].DS.$product['Product']['picture'];
				$ativo = ($product['Product']['id']==$id)? "active" : "";
			?>
			<div class="item <?=$ativo;?>">
				<figure><img src="<?=$img;?>" alt=""></figure>
				<table class="table table-bordered">
					<tr>
						<td colspan="9"><h3><?=$product['Product']['name'];?></h3></td>
					</tr>
					<tr>
						<td class="titulo">Leite Min</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
					<tr>
						<td class="titulo">Nome</td>
						<td>Leite Min</td>
					</tr>
				</table>
				</table>
			</div>
			<?php
			endforeach;
			?>
	</div>
	<script>
		$('.carousel').carousel();
	</script>