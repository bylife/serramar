<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Newspapper', array('class' => 'form-horizontal', 'type'=>'file'));?>
			<fieldset>
				<legend>O Cooperador - Cadastrar</legend>
				<?php
				echo $this->BootstrapForm->input('title', array(
					'required' => 'required',
					'label'=>'Título',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('code', array(
					'required' => 'required',
					'label'=>'Código do Título',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('picture', array(
					'label' => 'Imagem de capa',
					'type'=>'file',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('picture_path', array(
					'type' => 'hidden',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				?>
				<?php echo $this->BootstrapForm->submit('Salvar');?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Newspapper.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Newspapper.id'))); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Newspappers')), array('action' => 'index'));?></li>
		</ul>
		</div>
	</div>
</div>