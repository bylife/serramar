<div class="row-fluid">
	<div class="span9">
		<h2>O Cooperador - Edição #<?php echo h($newspapper['Newspapper']['id']); ?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['id']); ?>
			</dd>
			<dt><?php echo __('Title'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['title']); ?>
			</dd>
			<dt><?php echo __('Code'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['code']); ?>
			</dd>
			<dt><?php echo __('Picture'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['picture']); ?>
			</dd>
			<dt><?php echo __('Picture Path'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['picture_path']); ?>
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($newspapper['Newspapper']['created']); ?>
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Newspapper')), array('action' => 'edit', $newspapper['Newspapper']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Newspapper')), array('action' => 'delete', $newspapper['Newspapper']['id']), null, __('Are you sure you want to delete # %s?', $newspapper['Newspapper']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Newspappers')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Newspapper')), array('action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

