<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Newspapper', array('class' => 'form-horizontal', 'type'=>'file'));?>
			<fieldset>
				<legend>O Cooperador - Cadastrar</legend>
				<?php
				echo $this->BootstrapForm->input('title', array(
					'required' => 'required',
					'label'=>'Título',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('code', array(
					'required' => 'required',
					'label'=>'Código do Título',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('picture', array(
					'required' => 'required',
					'label' => 'Imagem de capa',
					'type'=>'file',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('picture_path', array(
					'required' => 'required',
					'type' => 'hidden',
					'helpInline' => '<span class="label label-important">' . 'Obrigatório' . '</span>&nbsp;')
				);
				?>
				<?php echo $this->BootstrapForm->submit('Cadastrar');?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link('Listar Edições do O Cooperador', array('action' => 'index'));?></li>
		</ul>
		</div>
	</div>
</div>