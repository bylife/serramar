<div class="row-fluid">
	<div class="span9">
		<h2>O Cooperador</h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

			<ul class="thumbnails">
		<?php foreach ($newspappers as $newspapper): ?>
		<?php $img = $this->webroot.'files'.DS.'newspapper'.DS.'picture'.DS.$newspapper['Newspapper']['picture_path'].DS.'thumb_';?>
		<li class="span2">
				<div class="thumbnail">
					<img src="<?php echo $img.$newspapper['Newspapper']['picture'];?>" alt="<?php echo $newspapper['Newspapper']['title']; ?>" class="img-polaroid">
				
				<p><?php echo $newspapper['Newspapper']['title']; ?></p>
				<p>
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $newspapper['Newspapper']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $newspapper['Newspapper']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $newspapper['Newspapper']['id']), null, __('Are you sure you want to delete # %s?', $newspapper['Newspapper']['id'])); ?>
				</p>
				</div>
		</li>
					
		<?php endforeach; ?>
		</ul>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link('Cadastar O Cooperador', array('action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>