<div id="main">
	<div class="row-fluid" id="freelac">
		<img src="<?php echo $this->webroot;?>img/freelac.png" alt="">
		<div class="content">
			<div class="row-fluid">
				<div class="video">
					<iframe width="100%" height="100%" src="//www.youtube.com/embed/m0FjTA5DIfk?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="row-fluid">
				<div class="posts">
					<div class="span6">
						<div class="row-fluid thumb">
							<figure><a href="http://www.serramar.coop.br/blog/intolerancia-a-lactose-cuidados-com-a-alimentacao/"><img src="<?php echo $this->webroot;?>img/artigo_cooperado.jpg" alt=""></a></figure>
						</div>
						<div class="row-fluid excerpt">
							<p><a href="http://www.serramar.coop.br/blog/intolerancia-a-lactose-cuidados-com-a-alimentacao/">Após consumir leite ou seus derivados, algumas pessoas podem sentir dor abdominal, náuseas, desconforto, diarreia e gases.</a></p>
						</div>
						<div class="row-fluid leia-mais">
							<div class="span8"><hr></div>
							<div class="span4"><a href="http://www.serramar.coop.br/blog/intolerancia-a-lactose-cuidados-com-a-alimentacao/">Leia +</a></div>
						</div>
					</div>
					<div class="span6">
						<div class="row-fluid thumb">
							<figure><a href="http://www.serramar.coop.br/blog/conheca-mais-sobre-o-leite-serramar-freelac/"><img src="<?php echo $this->webroot;?>img/serramar_027.jpg" alt=""></a></figure>
						</div>
						<div class="row-fluid excerpt">
							<p><a href="http://www.serramar.coop.br/blog/conheca-mais-sobre-o-leite-serramar-freelac/">Freelac é o primeiro leite pasteurizado do Brasil com baixo teor de Lactose, proporcionando aos intolerantes à lactose o prazer de consumir leite fresco</a></p>
						</div>
						<div class="row-fluid leia-mais">
							<div class="span8"><hr></div>
							<div class="span4"><a href="http://www.serramar.coop.br/blog/conheca-mais-sobre-o-leite-serramar-freelac/">Leia +</a></div>
						</div>
					</div>
				</div>
				<div class="arrow">
					<a href="">
						<?=$this->Html->image('seta-branca.png'); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid" id="light">
		<div class="content">
			<div class="row-fluid">
				<div class="video">
					<iframe width="100%" height="100%" src="//www.youtube.com/embed/ZCCH98IQQRU?rel=0" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="row-fluid">
				<div class="posts">
					<div class="span6">
						<div class="row-fluid thumb">
							<figure><a href="http://www.serramar.coop.br/blog/saiba-mais-sobre-o-novo-leite-serramar-light/"><img src="<?php echo $this->webroot;?>img/serramar_0263.jpg" alt=""></a></figure>
						</div>
						<div class="row-fluid excerpt">
							<p><a href="http://www.serramar.coop.br/blog/saiba-mais-sobre-o-novo-leite-serramar-light/">O novo leite Light da Serramar tem redução de 50% de gorduras (em relação ao leite integral) é ideal para quem precisa controlar a ingestão de calorias e gorduras.</a></p>
						</div>
						<div class="row-fluid leia-mais">
							<div class="span8"><hr></div>
							<div class="span4"><a href="http://www.serramar.coop.br/blog/saiba-mais-sobre-o-novo-leite-serramar-light/">Leia +</a></div>
						</div>
					</div>
					<div class="span6">
						<div class="row-fluid thumb">
							<figure><a href="http://www.serramar.coop.br/blog/?p=362"><img src="<?php echo $this->webroot;?>img/02_dicasparaviverlight.jpg" alt=""></a></figure>
						</div>
						<div class="row-fluid excerpt">
							<p><a href="http://www.serramar.coop.br/blog/?p=362">Tenha uma alimentação variada e que lhe dê prazer,  porém sem excessos. Pessoas que seguem dietas restritivas normalmente voltam a engordar.</a></p>
						</div>
						<div class="row-fluid leia-mais">
							<div class="span8"><hr></div>
							<div class="span4"><a href="http://www.serramar.coop.br/blog/?p=362">Leia +</a></div>
						</div>
					</div>
				</div>
				<div class="arrow">
					<a href="">
						<?=$this->Html->image('seta-blue.png'); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>