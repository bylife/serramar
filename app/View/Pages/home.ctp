<div id="main" class="corpo">
		<div class="container">

			<div class="rows posts">
				<section class="blog">
				<?php echo $this->Element('Blog/widget-home');?>
				</section><!-- .blog -->
			</div>

			<div class="rows">
				<section class="span6 produtos">
					<header>
						<h4>Produtos SERRAMAR</h4>
					</header>
					<figure><a href="#produtos"><img src="img/post_produtos.jpg" alt=""></a></figure>
					<aside>
						<p>
							Conheça a linha de produtos Serramar.
						</p>
					</aside>
				</section><!-- .produtos -->
				
				<section class="span6 fazendas">
					<header>
						<h4>Fazendas SERRAMAR</h4>
					</header>
					<figure><a href="<?= $this->Html->url(array('controller' => 'farms', 'action' => 'index')); ?>"><img src="img/post_fazenda.jpg" alt=""></a></figure>
					<aside>
						<p>
							Saiba um pouco mais sobre as fazendas e produtores que fazem nossos deliciosos produtos.
						</p>
					</aside>
				</section><!-- .fazendas -->
				
				<section class="span6 canal">
					<header>
						<h4>Canal no Youtube</h4>
					</header>
					<div class="content">
						<object width="420" height="315"><param name="movie" value="http://www.youtube.com/v/m0FjTA5DIfk?version=3&amp;hl=pt_BR"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/m0FjTA5DIfk?version=3&amp;hl=pt_BR" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>
					</div>
					<aside>
						<p>
							<a href="http://www.youtube.com/user/serramarlaticinios/videos">Acesse nosso canal no YouTube&reg; e veja nosso vídeos.</a>
						</p>
					</aside>
				</section><!-- .canal -->
				
				<section class="span6 facebook">
					<header>
						<h4>Curta nosso Facebook</h4>
					</header>
					<div class="content">
						<div class="fb-like-box" data-href="https://www.facebook.com/serramarlaticinios" data-width="400" data-height="330" data-show-faces="true" data-stream="true" data-header="false"></div>
					</div>
				</section>
			</div>
		
		</div><!-- .container -->
	</div><!-- #main -->

	<div id="historia" class="corpo">
		<div class="container">
			<div class="rows">
				<div class="span6 logo">
					<img src="img/logo_serramar.png" alt="">
				</div>
				<div class="span6 tabs">
					<!-- tabs -->
					<ul class="nav nav-tabs">
						<li class="blue"><a class="active" href="#tabs-historia" data-toggle="tab">História</a></li>
						<li class="yellow"><a href="#tabs-missao" data-toggle="tab">Missão e Visão</a></li>
						<li class="dark-blue"><a href="#tabs-conheca" data-toggle="tab">Conheça a SERRAMAR</a></li>
					</ul>
					<!-- contents -->
					<div class="tab-content">
						<div id="tabs-historia" class="tab-pane fade active in">
							<p>
								A Cooperativa de Laticínios SERRAMAR (antiga Cooperativa de Laticínios de Guaratinguetá), fundada em 02 de Abril de 1944, possui hoje mais de 800 cooperados ativos, reunindo produtores de diversos municípios do interior do estado de Sao Paulo - Guaratinguetá, Aparecida, Arapeí, Areias, Bananal, Cachoeira Paulista, Canas, Cruzeiro, Cunha, Lagoinha, Lavrinhas, Lorena, Pindamonhangaba, Piquete, Potim, Queluz, Roseira e São José do Barreiro.
							</p>
							<p>
								Produz atualmente mais de 65 milhões de litros de leite, contando com uma ampla rede de distribuição, levando a saúde do campo a regiões como o Vale do Paraíba, Campinas, Região Bragantina, Região Sul Fluminense, Litoral Norte, São Paulo e Grande São Paulo. 
							</p>
							<p>
								Em uma moderna unidade industrial, reinaugurada em dezembro de 2012, o leite das fazendas é processado pelas mais modernas tecnológicas, garantindo e originando produtos da mais alta qualidade, das marcas Serra Mar, Milk Mix e Maringá. 
							</p>
							<p>
								A SERRAMAR está continuamente em busca de novos desafios, investindo, crescendo e ampliando sua posição no cenário nacional, alicerçada sempre no relacionamento com seus cooperados, clientes, funcionários e colaboradores.
							</p>							

						</div>
						
						<div id="tabs-missao" class="tab-pane fade">
							<p>
								Uma cooperativa precisa ter uma visão do todo, mas sem nunca esquecer de cada um. Para isso, a modernização e o desenvolvimento da Cooperativa precisa estar baseada nos seguintes princípios.
							</p>
							<p>
								1 – Maior produtividade das fazendas cooperadas, através de:
							</p>
								<blockquote>- assistência técnica;</blockquote>
								<blockquote>- menor custo de insumos;</blockquote>
								<blockquote>- maior estabilidade na remuneração pela produção.</blockquote>
							<p>
								2 – Reestruturação administrativa e industrial para adequação de custos e procedimentos.
							</p>
							<p>
								3 – Ampliação das áreas de atuação comercial.
							</p>
							<p>
								4 – Busca de novos produtos para se obter ganhos estáveis e constantes, independentes dos concorrentes comuns do mercado próximo.
							</p>
						</div>
						<div id="tabs-conheca" class="tab-pane fade">
							<p>
								Quer conhecer a fábrica da Serramar? Faça o Tour Virtual e conheça nossa fábrica sem sair de casa.
							</p>
						</div>
					</div>

				</div><!-- .tabs -->
			</div>
		</div>
	</div><!-- #historia -->

	<section id="produtos" class="corpo">
		<div class="container">
			<header class="rows"><span>Produtos</span></header>
			<div class="rows content">
				<article id="carousel-serramar" class="slide serramar span4"><!-- tirei a .carousel -->
					<div class="rows logo">
						<img src="img/logo_serramar.png" alt="">
					</div>
					<!-- <a href="#carousel-serramar" data-slide="prev" class="seta prev"></a> -->
					<!-- <a href="#carousel-serramar" data-slide="next" class="seta next"></a> -->
					<div class="carousel-inner rows content">
						<div class="item ativo">
							<a class="figure" href="<?= $this->Html->url(array('controller' => 'products', 'action' => 'serramar')); ?>">
								<img src="<?= $this->webroot; ?>img/leite_min.png">
							</a>
							<p>
								Conheça a linha de produtos feitos com o mais delicioso leite das fazendas, um privilégio que você pode ter todos os dias em sua casa
							</p>
						</div>
						<?php // echo $this->Element('Product/serramar');?>
					</div>
				</article><!-- .serramar -->
				<article id="carousel-mix" class="slide mix span4"><!-- tirei a .carousel -->
					<div class="rows logo">
						<img src="img/logo_milk_mix.png" alt="">
					</div>
					<!-- <a href="#carousel-mix" data-slide="prev" class="seta prev"></a> -->
					<!-- <a href="#carousel-mix" data-slide="next" class="seta next"></a> -->
					<div class="carousel-inner rows content">
						<div class="item ativo">
							<a class="figure" href="<?= $this->Html->url(array('controller' => 'products', 'action' => 'milkmix')); ?>">
								<img src="<?= $this->webroot; ?>img/conjunto_milk_mix.png">
							</a>
							<p>
								A saborosa bebida láctea da Serramar em novos e deliciosos sabores: ameixa e laranja.  Saiba mais!
							</p>
						</div>
						<?php // echo $this->Element('Product/milkmix');?>
					</div>
				</article><!-- .mix -->
				<article id="carousel-maringa" class="slide maringa span4"><!-- tirei a .carousel -->
					<div class="rows logo">
						<img src="img/logo_maringa.png" alt="">
					</div>
					<!-- <a href="#carousel-maringa" data-slide="prev" class="seta prev"></a> -->
					<!-- <a href="#carousel-maringa" data-slide="next" class="seta next"></a> -->
					<div class="carousel-inner rows content">
						<div class="item ativo">
							<a class="figure" href="<?= $this->Html->url(array('controller' => 'products', 'action' => 'maringa')); ?>">
								<img src="<?= $this->webroot; ?>img/leite_maringa.png">
							</a>
							<p>
								A tradição que põe um sabor especial na sua mesa.
							</p>
						</div>
						<?php // echo $this->Element('Product/maringa');?>
					</div>
				</article><!-- .maringa -->
			</div><!-- .content -->
		</div>
	</section><!-- #produtos -->

	<section id="cooperativa" class="corpo">
		<div class="container">
			<header class="rows"><span>Cooperativa</span></header>
			<div class="rows content">
				<div class="span6">
			
					<div class="rows fazenda">
						<h3>Fazendas Serramar</h3>
						<p>
							Aqui você vai conhecer algumas das fazendas que representam o alto grau de excelência dos nossos produtores.
						</p>
						<?php echo $this->Element('Farm/farms');?>
					</div><!-- .rows.fazendal -->
			
					<div class="personagem">
						<img src="img/personagem_cooperativa.png" alt="">
					</div>
				</div>
			
				<div class="span6">
					<div class="rows produtor area">
						<h3>Área do Produtor</h3>
						<div class="container">
							<p>Área de acesso restrito</p>
							<p>
								Sistema de acesso a dados sigilosos.
							</p>
							<div class="form-horizontal">
								<form id="form-produtor" action="">
									<input type="hidden" name="produtor" id="produtor-hidden">
									<label for="produtor-codigo">Código de acesso:</label>
									<input type="text" name="codigo" id="produtor-codigo">
									<label for="produtor-senha">Senha de acesso:</label>
									<input type="text" name="senha" id="produtor-senha">
									<button type="submit">Enviar</button>
								</form>
							</div>
						</div>
					</div><!-- .rows.produtor -->

					<div class="rows jornal">
						<h3>Jornal – O Cooperador</h3>
						<p>
							Leia o informativo que promove conteúdo ao produto rural.
						</p>
						<?php echo $this->Element('Newspapper/newspapper');?>
					</div>
				</div>
			</div>
		</div>
	</section><!-- #cooperativa -->

	<section id="varejistas" class="corpo">
		<div class="container">
			<header class="rows"><span>Varejistas</span></header>
			<div class="rows content">
				<div class="span6 cadastrar">
					<figure><img src="img/cadastrar_verejistas.jpg" alt=""></figure>
					<article>
						<p>
							Encante os seus clientes! Ofereça a eles a alta qualidade dos produtos SERRAMAR. Solicite, pelo cadastro ao lado, a visita dos nossos representantes.
						</p>
					</article>
				</div>
				<div class="span6 cadastre-se area">
					<div class="container">
						<p>Cadastre-se aqui para receber a visita dos nossos representantes.</p>
						<div class="form-horizontal">
							<?php  
								/*echo $this->BootstrapForm->create('Retailer' , array('class' => 'form-horizontal',  'inputDefaults' => array( 'wrapInput' => false, 'div' => false, 'label' => false, 'class' => 'form-control')));
								echo $this->BootstrapForm->input('name', array('label' => 'Nome'));
								echo $this->BootstrapForm->input('Email', array('label' => 'Email'));
								echo $this->BootstrapForm->input('Telefone', array('label' => 'Tel'));
								echo $this->BootstrapForm->submit(__('Cadastrar'));
								echo $this->BootstrapForm->end();*/
							 ?>
							<form id="form-restrito" action="retailers/cadastro_varejistas">
								<label for="varejista-nome">Nome</label>
								<input type="text" name="data[Retailer][name]" id="varejista-nome">
								<label for="varejista-email">E-mail</label>
								<input type="text" id="varejista-email" name="data[Retailer][email]">
								<label for="varejista-telefone">Telefone	</label>
								<input type="text" id="varejista-telefone" name="data[Retailer][phone]">
								<label for="varejista-endereco">Endereço	</label>
								<input type="text" id="varejista-endereco" name="data[Retailer][address]">
								<label for="varejista-cidade">Cidade / UF </label>
								<input type="text" id="varejista-cidade" name="data[Retailer][city]">
								<button type="submit" id="cad-varejista">Cadastrar</button>
							</form> 
						</div>
					</div>
				</div>
				<div class="span6 restrito area">
					<div class="container">
						<p>Área de acesso restrito</p>
						<p>
							Sistema de acesso a dados sigilosos.
						</p>
						<div class="form-horizontal">
							<form id="form-restrito" action="">
								<input type="hidden" name="restrito" id="restrito-hidden">
								<label for="restrito-email">E-mail:</label>
								<input type="text" name="email" id="restrito-email">
								<label for="restrito-senha">Senha de acesso:</label>
								<input type="text" name="senha" id="restrito-senha">
								<button type="submit" id="btnvar">Enviar</button>
							</form>
						</div>
					</div>
				</div>
				<div class="personagens"><img src="img/varejistas.png" alt=""></div>
			</div>
		</div>
	</section><!-- #varejistas -->

	<section id="contato" class="corpo">
		<div class="container">
			<header class="rows"><figure><img src="img/icon_contato.png" alt=""></figure></header>
			<div class="rows content">
				<div class="span6 fale">
					<h3>Fale Conosco</h3>
					<p>
						Entre em contato conosco, envie suas dúvidas, críticas ou sugestões.
					</p>
					<p>
						Avenida da Exposição, 353 - Vila Regina - Cx. Postal 23 - Guaratinguetá/SP
						CEP: 12522-190
						SAC: 0800-7717116
					</p>
					<div class="social">
						<a href="http://www.facebook.com/serramarlaticinios"><i class="facebook"></i></a>						
						<a href="http://www.youtube.com/user/serramarlaticinios/videos"><i class="youtube"></i></a>
					</div>
				</div><!-- .span6.fale -->
				<div id="contato-area" class="span6 fields">
					<div id="loading" style="display: none">
						<?=$this->Html->image('bottle-milk.gif', array('style'=>'display: block; margin: 1px auto;'));?>
					</div>
					<?php
					$data = $this->Js->get('#form-contato')->serializeForm(array('isForm' => true, 'inline' => true));
					 $this->Js->get('#form-contato')->event(
          						'submit',
          						$this->Js->request(
				            		array('controller'=>'users', 'action' => 'contato'),
				            		array(
				                    	'update' => '#contato-area',
				                    	'before' => "$('#loading').fadeIn();",
				                    	'complete' => "$('#loading').fadeOut();",
				                    	'data' => $data,
				                    	'async' => true,    
				                    	'dataExpression'=>true,
				                    	'method' => 'POST'
				    				)
				   				)
				    );
					?>
						<?=$this->Form->create('User', array('action'=>'contato', 'default'=>false, 'class'=>'form-vertical', 'id'=>'form-contato', 'name'=>'form-contato'));?>
						<div class="control-group">
							<div class="control-label"><label for="fale-nome">Nome:</label></div>
							<div class="controls">
								<?=$this->Form->input('nome', array( 'label'=>false, 'div'=>false, 'id'=>'fale-nome'));?>								
							</div>
						</div>						
						<div class="control-group">
							<div class="control-label"><label for="fale-email">E-mail:</label></div>
							<div class="controls">
								<?=$this->Form->input('email', array( 'type'=>'email', 'label'=>false, 'div'=>false, 'id'=>'fale-email'));?>								
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><label for="fale-tel">Telefone:</label></div>
							<div class="controls">
								<?=$this->Form->input('tel', array( 'type'=>'tel', 'label'=>false, 'div'=>false, 'id'=>'fale-tel'));?>								
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><label for="fale-cidade">Cidade:</label></div>
							<div class="controls">
								<?=$this->Form->input('cidade', array( 'label'=>false, 'div'=>false, 'id'=>'fale-cidade'));?>								
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><label for="fale-assunto">Assunto:</label></div>
							<div class="controls">
								<?=$this->Form->input('assunto', array( 'label'=>false, 'div'=>false, 'id'=>'fale-assunto'));?>																
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><label for="fale-mensagem">Mensagem:</label></div>
							<div class="controls">
								<?=$this->Form->input('mensagem', array('type'=>'textarea', 'label'=>false, 'div'=>false, 'cols'=>'30', 'rows'=>'5', 'id'=>'fale-mensagem'));?>								
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<?=$this->Form->submit('Enviar', array('class'=>'submit','div'=>false, 'id'=>'btn_contato'));?>								
							</div>
						</div>
					<?=$this->Form->end();?>
					<?=$this->Js->writeBuffer();?>
				</div><!-- .span6.fields -->
			</div>
		</div>
	</section><!-- #contato -->

	<!-- Modals dos produtos -->
	<div id="modal-serramar" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="label-modal-serramar" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="label-modal-serramar">
				<?php echo isset($page_title) ? $page_title : 'Serramar'; ?>
			</h3>
		</div><!-- .modal-header -->
		<div class="modal-body"></div><!-- .modal-body -->
	</div><!-- #modal-serramar -->

	<!-- Modal do Formulário de Cadastrar Varejistas -->
	<div id="modal-varejistas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="label-modal-cadastrar" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
				×
			</button>
			<h3 id="label-modal-cadastrar">
				Cadastrar Varejistas
			</h3>
		</div><!-- .modal-header -->
		<div class="modal-body">

			<form id="form-varejistas" class="form-horizontal" action="<?=$this->Html->url('/cadastrar-varejista/');?>" method="post" accept-charset="utf-8">
				<div class="control-group">
					<label class="control-label" for="modal-nome">
						Nome:
					</label>
					<div class="controls">

						<input type="text" id="modal-nome" name="data[name]">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="modal-email">
						E-mail:
					</label>
					<div class="controls">

						<input type="text" id="modal-email" name="data[email]">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="modal-login">
						Login:
					</label>
					<div class="controls">

						<input type="text" id="modal-login" name="data[login]">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="modal-senha">
						Senha:
					</label>
					<div class="controls">

						<input type="password" id="modal-senha" name="data[password]">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="modal-repetir">
						Repetir a senha:
					</label>
					<div class="controls">

						<input type="password" id="modal-repetir" name="data[password-confirm]">
					</div>
				</div>
			</form>
		</div><!-- .modal-body -->
		<div class="modal-footer">
			<a href="#" id="cad-varejista" class="btn-cadastrar">
				Cadastrar
			</a>
		</div><!-- .modal-body -->
	</div><!-- #modal-cadastrar -->