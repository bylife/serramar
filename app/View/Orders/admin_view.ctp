<div class="row-fluid">
	<div class="span9">
		<h2>Pedidos</h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($order['Order']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('User'); ?></dt>
			<dd>
				<?php echo $this->Html->link($order['User']['name'], array('controller' => 'users', 'action' => 'view', $order['User']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Total'); ?></dt>
			<dd>
				<?php echo h($order['Order']['total']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($order['Order']['created']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Order')), array('action' => 'edit', $order['Order']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Order')), array('action' => 'delete', $order['Order']['id']), null, __('Are you sure you want to delete # %s?', $order['Order']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Orders')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Order')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Products')), array('controller' => 'products', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
<div>

<div class="row-fluid">
	<div class="span9">
		<h3><?php echo __('Related %s', __('Products')); ?></h3>
	<?php if (!empty($order['Product'])):?>
		<table class="table">
			<tr>
				<th><?php echo __('Id'); ?></th>
				<th><?php echo __('Name'); ?></th>
				<th><?php echo __('Code'); ?></th>
				<th><?php echo __('Storage'); ?></th>
				<th><?php echo __('Temperature'); ?></th>
				<th><?php echo __('Expiration'); ?></th>
				<th><?php echo __('Price'); ?></th>
				<th><?php echo __('Picture'); ?></th>
				<th><?php echo __('Path'); ?></th>
				<th><?php echo __('Unit'); ?></th>
				<th><?php echo __('Package'); ?></th>
				<th><?php echo __('Portions'); ?></th>
				<th><?php echo __('Line'); ?></th>
				<th><?php echo __('Created'); ?></th>
				<th><?php echo __('Updated'); ?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($order['Product'] as $product): ?>
			<tr>
				<td><?php echo $product['id'];?></td>
				<td><?php echo $product['name'];?></td>
				<td><?php echo $product['code'];?></td>
				<td><?php echo $product['storage'];?></td>
				<td><?php echo $product['temperature'];?></td>
				<td><?php echo $product['expiration'];?></td>
				<td><?php echo $product['price'];?></td>
				<td><?php echo $product['picture'];?></td>
				<td><?php echo $product['path'];?></td>
				<td><?php echo $product['unit'];?></td>
				<td><?php echo $product['package'];?></td>
				<td><?php echo $product['portions'];?></td>
				<td><?php echo $product['line'];?></td>
				<td><?php echo $product['created'];?></td>
				<td><?php echo $product['updated'];?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('controller' => 'products', 'action' => 'view', $product['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('controller' => 'products', 'action' => 'edit', $product['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'products', 'action' => 'delete', $product['id']), null, __('Are you sure you want to delete # %s?', $product['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
	<?php endif; ?>

	</div>
	<div class="span3">
		<ul class="nav nav-list">
			<li><?php echo $this->Html->link(__('New %s', __('Product')), array('controller' => 'products', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
