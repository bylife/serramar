<div class="row-fluid">
	<div class="span9">
		<?php echo $this->BootstrapForm->create('Order', array('class' => 'form-horizontal'));?>
			<fieldset>
				<legend>Novo Pedido</legend>
				<?php
				echo $this->BootstrapForm->input('user_id', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				echo $this->BootstrapForm->input('total');
				echo $this->BootstrapForm->input('product_id', array(
					'required' => 'required',
					'helpInline' => '<span class="label label-important">' . __('Required') . '</span>&nbsp;')
				);
				?>
				<?php echo $this->BootstrapForm->submit(__('Submit'));?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header">Ações</li>
			<li><?php echo $this->Html->link('Pedidos', array('action' => 'index'));?></li>
			<li><?php echo $this->Html->link('Usuários', array('controller' => 'users', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Novo Usuário', array('controller' => 'users', 'action' => 'add')); ?></li>
			<li><?php echo $this->Html->link('Pedidos', array('controller' => 'products', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Novo Produto', array('controller' => 'products', 'action' => 'add')); ?></li>
		</ul>
		</div>
	</div>
</div>