<div class="row-fluid">
	<div class="span9">
		<h2>Varejistas</h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th></th>
			</tr>
		<?php foreach ($retailers as $dado):?>
			<tr>
				<th><?php echo $dado['Retailer']['id']; ?></th>
				<th><?php echo $dado['Retailer']['name']; ?></th>
				<th><?php echo $dado['Retailer']['email']; ?></th>
				<th><?php echo $dado['Retailer']['phone']; ?></th>
				<th><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $dado['Retailer']['id']), array('class'=>'btn btn-mini'), __('Tem certeza que deseja excluir este Varejista?')); ?></th>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		
	</div>
</div>