<?php
App::uses('AppModel', 'Model');
/**
 * Farm Model
 *
 */
class Farm extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'picture' => array(
                'fields' => array(
                    'dir' => 'path_picture'
                ),
                'thumbnailSizes' => array(
                    'medium' => '450x300',
                    'thumb' => '144x90'
                )
            )
        ),
        'Sluggable' =>  array( 'title_field' => 'title', 'slug_field' => 'slug' )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'body' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
}
