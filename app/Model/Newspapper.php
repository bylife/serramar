<?php
App::uses('AppModel', 'Model');
/**
 * Newspapper Model
 *
 */
class Newspapper extends AppModel {

	public $actsAs = array(
        'Upload.Upload' => array(
            'picture' => array(
                'fields' => array(
                    'dir' => 'picture_path'
                ),
                'thumbnailSizes' => array(
                    'vga' => '420x665',
                    'thumb' => '75x110'
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
}
