<?php
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 * @property Information $Information
 * @property OrdersItem $OrdersItem
 */
class Product extends AppModel {
	public $actsAs = array(
        'Upload.Upload' => array(
            'picture' => array(
                'fields' => array(
                    'dir' => 'path'
                ),
                'thumbnailSizes' => array(
                	'real' => '200x300'
                )
            )
        )
    );

    public function getTabelaNutricional(){
    	$tabelanutricional = "<table border='1' width='450px'><tr><td colspan='3'> <center><h3>INFORMAÇÃO NUTRICIONAL</h3><h4>Porção de [informar valor da porção]</h4></td></tr><tr><td colspan='2'><center>Quantidade por porção</td> <td><center>%VD (*)</td></tr><tr><td>Valor Energético</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Carboidratos, dos quais:</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Lactose</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Frutose</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Sacarose</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Proteínas</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Gorduras Totais, das quais:</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Gorduras Saturadas</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Gorduras Trans</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Gorduras Monoinsaturadas</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Gorduras Poliinsaturadas</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Cálcio</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td>Sódio</td><td><center>[informar]</td><td><center>[informar]</td></tr><tr><td colspan='3'><p>(*)% Valores Diários de referência com base em uma dieta de 2.000 kcal ou 8.400 kJ. Seus valores diários podem ser maiores ou menores dependendo de suas necessidades energéticas.</p><p>(**) Açúcares naturalmente presentes nas matérias-primas.</p><p>(***) Valores Diários não estabelecidos. </p></td></tr></table>";

    	return $tabelanutricional;
    }

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'unit' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'line' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Information' => array(
			'className' => 'Information',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'OrdersItem' => array(
			'className' => 'OrdersItem',
			'foreignKey' => 'product_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
