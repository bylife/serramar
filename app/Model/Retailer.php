<?php
App::uses('AppModel', 'Model');
App::uses('CakeEmail','Network/Email');
/**
 * Retailer Model
 *
 */
class Retailer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public function afterSave($created){
		$this->_sendmailVarejistas($this->data);
	}

	private function _sendmailVarejistas($dados){

		$email = new CakeEmail();

		$email->from('serramar@ondasete.com.br');			
      	$email->config('smtp');		
		$email->to('marcospaiva@clg.com.br');		
		$email->bcc('marketing@serramar.coop.br');		
		$email->subject('Serramar - Cadastro de Varejista');
		$email->template('email_cadastro');
		$email->emailFormat('html');
		$email->viewVars(array('dados' => $dados));
			
		if($email->send()){
			return true;
		} else {
			return false;
		}
	}

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'phone' => array(
			'phone' => array(
				'rule' => array('phone' , '^(?:(?:([0-9]{2}))?[-. ]?)?([0-9]{4})[-. ]?([0-9]{4})$^', 'pt_BR'),
				'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'address' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Por favor informe o endereço!',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'city' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
