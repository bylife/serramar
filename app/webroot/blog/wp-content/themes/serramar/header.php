<!DOCTYPE HTML>
<html lang="pt-br" xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="UTF-8">
	<title><?php bloginfo('title'); ?></title>
	
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.less" rel="stylesheet/less"></link>
	
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/libs/jquery.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/libs/less.js"></script>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/libs/plax/jquery.plax.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap-carousel.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap-transition.js"></script>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/functions.js"></script>

	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/loading-posts.js"></script>

	<script src="//connect.facebook.net/en_US/all.js"></script>
	<?php if(is_single()): the_post(); ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID) ); ?>
	<meta property="og:description" content="<?php echo strip_tags(get_the_excerpt());?>" />
	<meta property="og:image" content="<?php echo $image[0];?>" />			
	<?php endif;?>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <?php wp_head(); ?>

    <?php
    	// Construindo uma condicional para identificar a página
    	$current_page = '';
    	if (is_home()) $current_page = 'home';
    	if (is_search()) $current_page = 'search';
    	if (is_category()) $current_page = 'category';

    	// URL da página inicial
    	$initial_page_site = 'http://www.serramar.coop.br/';
    ?>

</head>
<body class="blog" page="<?php echo $current_page; ?>">

	<script>
		(function(d){
		var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/es_LA/all.js";
		d.getElementsByTagName('head')[0].appendChild(js);
		}(document));
	</script>

	<div id="fb-root"></div>
	<script>
		FB.init({
		appId      : 'YOUR_APP_ID',
		channelUrl : 'www.facebook.com/serramar', // Channel File
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
		});
	</script>

	<header class="corpo">
		<div class="wrap parallax-viewport">
			<div id="main-menu">
				<ul class="nav nav-pills">
					<li><a href="<?php echo $initial_page_site; ?>#historia">A Serramar</a></li>
					<li><a href="<?php echo $initial_page_site; ?>#produtos">Produtos</a></li>
					<li><a href="<?php echo $initial_page_site; ?>#cooperativa">Cooperativa</a></li>
					<li class="logo">
						<a href="<?php echo $initial_page_site; ?>">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_serramar.png" alt="Serramar">
						</a>
					</li>
					<li><a href="<?php echo site_url().'/';?>" id="sabor-saude">Blog</a></li>
					<li><a href="<?php echo $initial_page_site; ?>#varejistas">Varejistas</a></li>
					<li><a href="<?php echo $initial_page_site; ?>#contato">Fale conosco</a></li>
				</ul>
			</div><!-- #main-menu -->
			<div class="container">
				<div class="social">
					<a class="facebook" href="https://www.facebook.com/pages/SERRAMAR/199148920158111?fref=ts" alt="Acesse nossa Fanpage no Facebook"></a>
					<a class="youtube" href="http://www.youtube.com/channel/UCdUSjNjc9LHh9u2uVMHwbng?feature=watch" alt="Acesse nosso canal no Youtube"></a>
				</div><!-- .social -->
				<div id="mbox" class="carousel slide">
					<div class="carousel-inner content">
						<div class="item active">
							<a href="http://www.serramar.coop.br/lancamento">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg_blog_leite.jpg" alt="">
							</a>
						</div>
						<div class="item ">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg_blog_header.jpg" alt="">
						</div>
					</div>
				</div><!-- #mbox -->
			</div>
		</div>
		<div class="animation">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg_leite.png" alt="">
		</div>
		<div id="carousel-nav" class="nav">
			<ul>
				<li><a href="#1" class="active" data-to="1">&nbsp;</a></li>
				<li><a href="#2" class="" data-to="2">&nbsp;</a></li>
			</ul>
		</div>
	</header>