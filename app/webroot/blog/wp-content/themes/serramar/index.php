<?php
	get_header();
?>

	<div class="container">
		<div class="rows">
			<ul class="categorias">
				<?php serramar_show_categories(); ?>
			</ul>
			<?php get_search_form(); ?>
		</div>
		<div class="span12">
			<div class="span8">
				<?php get_template_part('loop'); ?>
			</div>
			<!-- .span4 -->
			<?php get_sidebar(); ?>
		</div>
	</div>

<?php
	get_footer();
?>