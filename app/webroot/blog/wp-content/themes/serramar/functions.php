<?php

	add_theme_support('post-thumbnails');
	
	add_action('after_setup_theme', 'serramar_setup_theme');

	// Ao iniciar o carregamento do tema
	function serramar_setup_theme() {
		// Carregando scripts
		add_action('wp_enqueue_scripts', 'serramar_scripts');

		// Criando hooks automáticos voltados ao Carregamento de posts por Ajax
		add_action('wp_ajax_more_news', 'serramar_more_news_ajax');
		add_action('wp_ajax_nopriv_more_news', 'serramar_more_news_ajax');
	}

	function serramar_more_news_ajax() {
		// VARIÁVEIS VINDAS DA REQUISIÇÃO AJAX
		$current_page = $_REQUEST['page'];
		// Número de posts à ignorar por já terem sidos carregados
		$offset = intval($_REQUEST['count_news']);
		// O id da categoria
		$cat_ID = $_REQUEST['cat'];
		// Variável criando pelo 'wp_localize_scrip()' ao carregar a página pelo hook 'wp_enqueue_scripts'
		$term_search = $_REQUEST['s'];

		$args = '';

		// Carregar todos os posts em filtros
		switch ($current_page) :
			case 'search' :
				$args .= 's=' . $term_search;
				break;
			case 'category' :
				$args .= 'cat=' . $cat_ID;
				break;
			default :
				// Valores padrão para consulta
				$args .= 'post_type=post';
		endswitch;

		// Valores padrão para todos as consultas
		$args .= '&posts_per_page=3&offset=' . $offset;

		// Procurar mais posts
		$more_news_posts = new WP_Query($args);

		// Loop
		if ($more_news_posts->have_posts()) :
			while ($more_news_posts->have_posts()) :
				$more_news_posts->the_post();

				// Imprimir resultado
				get_template_part('post');

			endwhile;
		else :
			get_template_part('post', 'error');
		endif;

		// Reset post data
		wp_reset_postdata();

		// Para função com EXIT
		// Impedir o Ajax de continuar o processo de Buscar por Resultado/Consulta
		exit;
	}

	// Carregando scripts
	function serramar_scripts() {
		wp_enqueue_script('functions', get_stylesheet_directory_uri() . '/js/functions.js');

		// Adicionando variável Ajax ao JS
		wp_localize_script('functions', 'serramar_ajax_url', admin_url('admin-ajax.php'));
		wp_localize_script('functions', 'serramar_ajax_search', $_REQUEST['s']);
	}

	function serramar_get_category($id, $name, $color) {
		$icon = '<span class="' . $color . '"></span>';
		$link = '<a href="' . get_category_link($id) . '">' . $name . '</a>';
		return $icon . $link;
	}

	function serramar_color_category($category) {
		$color_default = 'silver';

		switch ($category) :
			case 'sabor_saude' :
				$color_category = 'blue';
				break;
			case 'mae_filho' :
				$color_category = 'green';
				break;
			case 'receitas' :
				$color_category = 'pink';
				break;
			case 'noticias' :
				$color_category = 'orange';
				break;
			case 'freelac' :
				$color_category = 'orange2';
				break;
			case 'light' :
				$color_category = 'yellow';
				break;
			default :
				$color_category = $color_default;
		endswitch;

		return $color_category;
	}

	function serramar_show_categories() {
		// Bsucando categorias
		$categorias = get_categories('hide_empty=0');
		// Cor padrão
		$color_category_default = 'silver';
		// Loop
		foreach ($categorias as $categoria) :
			echo '<li>';
			switch ($categoria->slug) :
				case 'sabor_saude' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'blue');
					break;
				case 'mae_filho' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'green');
					break;
				case 'receitas' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'pink');
					break;
				case 'noticias' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'orange');
					break;
				case 'freelac' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'orange2');
					break;
				case 'light' :
					echo serramar_get_category($categoria->term_id, $categoria->name, 'yellow');
					break;
				default :
					echo serramar_get_category($categoria->term_id, $categoria->name, $color_category_default);
			endswitch;
			echo '</li>';
		endforeach;
	}

?>