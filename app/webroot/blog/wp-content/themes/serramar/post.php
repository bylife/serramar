	<?php
		$category_info = get_the_category(get_the_ID());
		$category_slug = $category_info[0]->slug;
	?>
	<div class="rows post <?php echo serramar_color_category($category_slug); ?>">
		<!-- Id da categoria para que a função Ajax possa rodar por completo -->
		<input type="hidden" class="post-category" name="post-category" value="<?php the_category_ID(); ?>">
		<header>
			<h2><?php the_title(); ?></h2>
		</header>
		<a href="<?php the_permalink(); ?>" class="figure"><?php the_post_thumbnail(); ?></a>
		<p>
			<?php the_content(); ?>
		</p>
		<footer>
			<div class="social">
				<iframe src="https://www.facebook.com/plugins/like.php?href=https://www.facebook.com/pages/SERRAMAR/199148920158111?fref=ts"
        scrolling="no" frameborder="0"
        style="border:none; width: 60px; height: 25px;"></iframe>
				<iframe allowtransparency="true" frameborder="0" scrolling="no"
  src="//platform.twitter.com/widgets/follow_button.html?screen_name=twitterapi&lang=pt-br"
  style="width:60px; height:20px; margin: 0 0 2px 0;"></iframe>
			</div>
			<?php
				$post_type = get_post_type(get_the_ID());
				if ($post_type == 'post') :
			?>
			<div class="categorias">
				<span>Categorias: </span>
				<span class="categoria"><?php the_category(' '); ?></span>
			</div>
			<?php endif; ?>
		</footer>
	</div>