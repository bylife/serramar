<?php
	if (have_posts()) :
		$show_num_posts = 0;
		while (have_posts()) : the_post();
			$show_num_posts++;
			if ($show_num_posts > 3) break;
			get_template_part('post');
		endwhile;
	else :
		get_template_part('post', 'error');
	endif;
?>
	<div id="loading">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/loading.gif" alt="">
		<p>Carregando mais posts</p>
	</div>