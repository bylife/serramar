	<div class="span4 sidebar">
		<div class="rows categorias">
			<header>Categorias</header>
			<div class="content">
				<ul>
					<?php
						echo wp_list_categories(
							array(
								'title_li'	=> '',
								'hide_empty'=> 0
							)
						);
					?>
				</ul>
			</div>
		</div>
		<div class="rows tags">
			<header>Tags</header>
			<div class="content">
				<?php wp_tag_cloud(); ?>
			</div>
		</div>
		<div class="rows facebook">
			<div class="content">
				<div class="fb-like-box" data-href="https://www.facebook.com/pages/SERRAMAR/199148920158111?fref=ts" data-width="230" data-show-faces="true" data-stream="false" data-header="true"></div>
			</div>
		</div>
	</div>