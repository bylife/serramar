jQuery(document).ready(function($) {

	/*
	 * @title Carregar posts automáticamente
	 * @param Quando a Scroll chegar ao fim, carregar mais posts
	 */
	var $window, $scroll, $loading_posts, $current_page, $not_posts, $tamanho_documento, $tamanho_scroll, $content_posts, $posts_carregados;
	// Local para jogar os dados retornados do Ajax
	$window = $(window);
	$not_posts = $('#not-posts');
	$loading_posts = $('#loading');
	$content_posts = $('body.blog > .container .span8');
	// Página atual que servirá de auxilio para a condicional da função Ajax dentro da functions.php
	$current_page = $('body').attr('page');
	// Quando carregar a página vai procurar quantos posts já foram exibidos
	// Depois ele vai ser atualizado dentro da requisição Ajax
	$posts_carregados = $content_posts.find('.post').length;
	// Chamar evento
	$window.scroll(function(scroll_event) {
		if ($posts_carregados > 2) {
			var $scroll = $(this);

			$tamanho_documento = $(document).height();
			$distancia_scroll = $scroll.scrollTop();
			$tamanho_scroll = $scroll.height();
			$distancia_final_scroll = $distancia_scroll + $tamanho_scroll;

			if (($distancia_final_scroll >= $tamanho_documento) && !scroll_event.isImmediatePropagationStopped()) {
				// Impedir de repetir as mesmas ações em uma única chamado do evento
				// Evitando bug de 'repetir ações como um loop' em uma única chamada
				scroll_event.stopImmediatePropagation();
				// Incluir o loading
				if (!$not_posts.length)
					$loading_posts.css({'visibility':'visible'});
				// A rolagem chegou ao fim
				// Procurar mais posts
				// Pegar variável com a URL do arquivo Ajax
				$url = serramar_ajax_url;
				// Enviar dados
				$.post(
					$url,
					{
						'action'	: 'more_news',
						'count_news': $posts_carregados,
						'page'		: $current_page,
						'cat'		: $content_posts.find('.post .post-category').val(),
						's'			: serramar_ajax_search
					},
					function(html, textStatus, jqXHR) {
						// Imprimir resultado se não existir o elemento de #not-posts
						if (!$not_posts.length) {
							// Adicionar os dados retornados antes do #loading e após o ultimo post
							$loading_posts.before(html);
							// Atualizar contador de notícias exibidas com base na variável a baixo
							$posts_carregados = $content_posts.find('.post').length;
						}
						// Remover loading
						$loading_posts.css({'visibility':'hidden'});
					},
					'html'
				);
				// Se não tiver mais posts
				// Retirar o elemento #loading já que ele não vai ser mais preciso
				$not_posts = $('#not-posts');
				if ($not_posts.length) $loading_posts.css({'display':'none'});
			}
		}
	});
});