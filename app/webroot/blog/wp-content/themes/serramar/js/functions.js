/*
 * @title Plugin
 * @name
 * @description
 */
(function($) {})(jQuery);

jQuery(document).ready(function($) {

	/*
	 * @title Carousel (Twitter Bootstrap)
	 * @param Criando a paginação do @title
	 */

	$('.carousel').carousel();

	$('#mbox').carousel({
  		interval: 3000
	});

	$('#carousel-nav > ul > li > a').click(function(q) {

		var item = Number($(this).attr('href').substring(1));

	    $('#mbox').carousel(item - 1);
	    $('#carousel-nav li a').removeClass('active');
	    $(this).addClass('active');

	    q.preventDefault();
	});

	$('#mbox').bind('slid', function() {

	    $('#carousel-nav li a').removeClass('active');
	    var idx = $('#mbox .item.active').index();
	    $('#carousel-nav li a:eq(' + idx + ')').addClass('active');

	});

	/*
	 * @description Campo SEARCH
	 * @param Quando o campo receber foco remover label
	 */
	var $field_search, $box_search, $label_search;

	$box_search = $('.search');
	$field_search = $box_search.find('input');
	$label_search = $box_search.find('label');

	// Quando acessar a página
	if($field_search.val() !== '') $label_search.hide();
	
	// Outros eventos
	$field_search.bind({
		focusin: function() { $label_search.hide(); },
		focusout: function() {
			if($field_search.val() === '') $label_search.show();
		}
	});
	
});