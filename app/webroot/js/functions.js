/**
 * @title Plugin
 * @name
 * @description
 */
(function($) {})(jQuery);

var bg_width = false;

function getImages() {
	return ['img/slide_home1.jpg', 'img/slide_home2.jpg', 'img/slide_home3.jpg', 'img/bg_cooperativa.jpg', 'img/bg_home_header.jpg', 'img/bg_fazendas_header.jpg', 'img/bg_historia.jpg', 'img/bg_varejistas.jpg', 'img/bg_produtos.jpg', 'img/logo_serrama.png', 'img/varejistas.png'];
}

$(function() {

	var $all, $body;

	$all = $('#all');
	$body = $('body');

	if ($body.hasClass('home')) {
		$body.addClass('overflow');
		$('html, body').scrollTop(0);
		$all.addClass('hidden');
		$.preloadImages(getImages(), function() {
			$('.loading').fadeOut(500);
			setTimeout(function() {
				$all.removeClass('hidden');
				$body.removeClass('overflow');
			}, 600);
		});
	}

});


jQuery(document).ready(function($) {

	/**
	 * @title Carousel (Twitter Bootstrap)
	 * @param Criando a paginação do @title
	 */

	$('.carousel').carousel();

	$('#mbox').carousel({
  		interval: 3000
	});

	$('#carousel-nav > li > a').click(function(q) {

	    var item = Number($(this).attr('href').substring(1));

	    $('#mbox').carousel(item - 1);
	    $('#carousel-nav li a').removeClass('active');
	    $(this).addClass('active');

	    q.preventDefault();
	});

	$('#mbox').bind('slid', function() {

	    $('#carousel-nav li a').removeClass('active');
	    var idx = $('#mbox .item.active').index();
	    $('#carousel-nav li a:eq(' + idx + ')').addClass('active');

	});

	/**
	 * @title Ativar todos os Modals
	 * @param Verificar se existe algum elemento 'modal' (evitar bug)
	 * @param Impedir de mostra-los ao inicilizar a página
	 */
	var $modal = $('.modal');
	var $modal_remote = $('.modal-remote');

	if ($modal.length > 0) {

		$modal.modal({
			show: false,
			remote: false
		});

	}

	if ($modal_remote.length > 0) {

		$modal_remote.modal({
			show: false,
			remote: true
		});

		var $modalRemote, $modalRemoteUrl, $modalRemoteContent;

		$modalRemote = $('.modal-remote');

		// Ao clicar no link do modal
		$modalRemote.click(function(event) {
			var $this;
			$this = $(this);
			// Pegar url do elemento clicado
			$modalRemoteUrl = $this.attr('href');
			// Pegar local de 'onde jogar o conteúdo do href'
			$modalRemoteContent = $modalRemote.attr('data-target');
			$modalRemoteContent = $($modalRemoteContent).find('.modal-body');
			// Carregar url e jogar no elemento indicado pelo link
			$modalRemoteContent.load($modalRemoteUrl);

			return event.preventDefault();
		});

	}

	/**
	 * @description Campo SEARCH
	 * @param Quando o campo receber foco remover label
	 */
	var $field_search, $box_search, $label_search;

	$box_search = $('.search');
	$field_search = $box_search.find('input');
	$label_search = $box_search.find('label');

	// Quando acessar a página
	if($field_search.val() !== '') $label_search.hide();
	
	// Outros eventos
	$field_search.bind({
		focusin: function() { $label_search.hide(); },
		focusout: function() {
			if($field_search.val() === '') $label_search.show();
		}
	});

	/**
	 * @title Field Group
	 * @param Mostrar sub-menus ao clicar no botão
	 * @param Ao clicar no link do sub-menu, pegar o valor e jogar no campo
	 */
	function FieldGroup() {
		var $fieldGroup, $dropDown, $link, $btn, $this, value;
		
		$btn = $('.field-group .btn');
		$link = $('.dropdown-menu a');

		$btn.click(function() {
			$this = $(this);
			return $this.parent().children('.dropdown-menu').toggleClass('show');
		});

		$link.click(function(event) {
			$this = $(this);
			event.preventDefault();
			value = $this.text();
			$this.parents('.field-group').find('.field').val(value);
			return $this.parents('.dropdown-menu').toggleClass('show');
		});

		return true;
	}

	FieldGroup();

	/**
	 * @title Parallax Scroll
	 * @param Efeito entre conteúdos/seções
	 */
	$('#main-menu .nav, #menu-nav, .localscroll').localScroll( 800 );

	/**
	 * Chamando a função que dispara o efeito de tabs
	 * Efeito surge em tudos elementos HTML que tem um atributo data-toggle automáticamente
	 */
	$('a[data-toggle="tab"]').on( 'shown', function (e) {
		e.target // activated tab
		e.relatedTarget // previous tab
	});
	
	/**
	 * @title Validar formulário de contato
	 */
	var $form_contato, $btn_contato;

	$form_contato = $('#form-contato');
	$btn_contato = $('#btn_contato');

	//$form_contato.on('submit', false);

	$btn_contato.click(function() {
		var $nome, $telefone, $assunto, $mensagem, $email, $cidade;
		

		var action = $form_contato.attr('action');
		$nome = $form_contato.find('#fale-nome');
		$telefone = $form_contato.find('#fale-tel');
		$cidade = $form_contato.find('#fale-cidade');
		$email = $form_contato.find('#fale-email');
		$assunto = $form_contato.find('#fale-assunto');
		$mensagem = $form_contato.find('#fale-mensagem');

		$telefone.mask('(99)9999-9999');

		if($nome.val() == '') {
			alert('O Campo nome deve ser preenchido.');
			$nome.focus();
			return false;
		}
		else if($email.val() == '' || !$email.val().match(/^([a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4}$)/i)) {
			alert('O Campo email deve ser preenchido corretamente');
			$email.focus();
			return false;
		}
		else if($telefone.val() == ''){
			alert('O campo Telefone deve ser preenchido corretamente');
			$telefone.focus();
			return false;
		}
		else if($cidade.val() == ''){
			alert('O campo cidade deve ser preenchido');
			$cidade.focus();
			return false;
		}
		else if($assunto.val() == '') {
			alert('O Campo assunto deve ser preenchido.');
			$assunto.focus();
			return false;
		} else if($mensagem.val() == '') {
			alert('O Campo mensagem deve ser preenchido.');
			$mensagem.focus();
			return false;
		}		

		$.post(
			action,
			{nome: $nome.val(), assunto: $assunto.val(), mensagem: $mensagem.val(), cidade: $cidade.val(), telefone: $telefone.val(), email: $email.val()},
			function(data){
				alert(data);
			},
			'json'
		);	
	});
	
	/**
	 * @title Validar formulários do PRODUTOR
	 */
	var $form_produtor, $btn_produtor;

	$form_produtor = $('#form-produtor');
	$btn_produtor = $form_produtor.find('[type="submit"]');

	$form_produtor.on('submit', false);

	$btn_produtor.click(function() {
		var $codigo, $senha;

		$codigo = $form_produtor.find('#produtor-codigo');
		$senha = $form_produtor.find('#produtor-senha');

		if($codigo.val() == '' && $senha.val()) {
			$codigo.focus();
			alert('Preencha todos os campos.');
			return false;
		}
		else if($codigo.val() == '') {
			alert('Qual o código de acesso?');
			$codigo.focus();
			return false;
		}
		else if($senha.val() == '') {
			alert('Qual a senha de acesso?');
			$senha.focus();
			return false;
		}
		else {
			if($form_contato.attr('action') != '') {
				$.post(
					$form_produtor.attr('action'),
					{ codigo: $codigo.val(), senha: $senha.val(), form: 'produtor' },
					function(data) {
						if(data)
							alert(data);
						else
							alert('Não foi possível enviar suas informações.');
					}
				);
			}
		}
	});
	
	/**
	 * @title Validar formulários do RESTRITO
	 */
	var $form_restrito, $btn_restrito;

	$form_restrito = $('#form-restrito');
	$btn_restrito = $form_restrito.find('[type="submit"]');

	$form_restrito.on('submit', false);

	$btn_restrito.click(function() {
		var $nome, $email, $telefone;

		$nome = $form_restrito.find('#varejista-nome');
		$email = $form_restrito.find('#varejista-email');
		$telefone = $form_restrito.find('#varejista-telefone');
		$endereco = $form_restrito.find('#varejista-endereco');
		$cidade = $form_restrito.find('#varejista-cidade');

		if($nome.val() == '') {
			$nome.focus();
			alert('Favor preencher seu nome');
			return false;
		}
		else if($email.val() == '' || !$email.val().match(/^([a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4}$)/i)) {
			alert('Por favor, preencha corretamente o seu e-mail.');
			$email.focus();
			return false;
		}
		else if($telefone.val() == '') {
			alert('Informe um telefone para contato.');
			$telefone.focus();
			return false;
		}
		else if($endereco.val() == '') {
			alert('Informe o endereço.');
			$endereco.focus();
			return false;
		}
		else if($cidade.val() == '') {
			alert('Informe a cidade  / Estado');
			$cidade.focus();
			return false;
		}
		
			if($form_restrito.attr('action') != '') {
				$.post(
					$form_restrito.attr('action'),
					{ nome: $nome.val(), email: $email.val(), telefone: $telefone.val(), endereco: $endereco.val(), cidade: $cidade.val(), form: 'restrito' },
					function(data) {
						if(data)
							alert('Informações cadastradas com sucesso, em breve entraremos em contato');
						else
							alert('Não foi possível enviar suas informações.');
					}
				);
			}
	});



	/**
	 */
	var $slide = function() {
		var $this, $window, $loop, $num_loop, $duration_loop, $pos_image, $el, $directory, $quant, $name, $ext, $duration, $delay, $images, $url, $site;

		// Inicializando variáveis
		$this = this;
		$window = $(window);
		// Após a primeira vez que o loop rodou, fazer um novo calculo de duração do loop
		$num_loop = 0;
		// O loop vai começar com imediatamente e depois será feito um novo calculo
		$duration_loop = 0;
		// Posição inicial do array das imagens
		$pos_image = 0;
		// Elemento onde vai ser inserido a imagem como background
		$el = $('#mbox');
		// Propriedades da imagem
		$ext = '.jpg';
		$name = 'slide_home';
		$directory = 'img/';
		// Propriedades do slide
		$quant = 3;
		$duration = 400;
		$delay = 5000;

		// Criar um array com os nomes das imagens
		$images = new Array($quant);
		// Pegar o nome da imagem e criar o nome das outras imagens
		for (var i = 0; i < $quant ; i++) {
			$images[i] = $name + (i + 1) + $ext;
		}

		$this._loop = function(pos_image) {
			// Criando a url/caminho para imagem
			$url = 'url( ' + $directory + $images[pos_image] + ' )';
			if(pos_image == 0){
				$el.parent('a').attr('href','http://www.serramar.coop.br/lancamento');				
			} else {
				$el.parent('a').attr('href','http://www.serramar.coop.br');				
			}
			return $el.animate({
				'opacity' : 0
			}, {
				duration: $duration,
				complete: function() {
					// Colocar a imagem como background no elemento
					$el.css({'background-image' : $url});
					// Aparecer elemento
					$el.animate(
						{ 'opacity' : 1 },
						{ duration: $duration }
					);
				}
			});
		};
		$duration_loop = $delay + ($duration * 2);
		$this._loop($pos_image);
		$pos_image++;
		// Loop infinito
		return $loop = setInterval(function() {
			// Chamando o loop após a primeira rodada
			$this._loop($pos_image);
			// Se chegou na ultima posição do array das imagens, volta para a primeira posição
			$pos_image++;
			return $pos_image = ($pos_image < $images.length) ? $pos_image : 0 ;
		}, $duration_loop);
	};

	//$slide();

});