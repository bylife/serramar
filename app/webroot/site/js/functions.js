/*
 * @title Plugin
 * @name
 * @description
 */
(function($) {})(jQuery);

jQuery(document).ready(function($) {

	/*
	 * @title Carousel (Twitter Bootstrap)
	 * @param Criando a paginação do @title
	 */

	$('.carousel').carousel();

	$('#mbox').carousel({
  		interval: 3000
	});

	$('#carousel-nav > li > a').click(function(q) {

	    var item = Number($(this).attr('href').substring(1));

	    $('#mbox').carousel(item - 1);
	    $('#carousel-nav li a').removeClass('active');
	    $(this).addClass('active');

	    q.preventDefault();
	});

	$('#mbox').bind('slid', function() {

	    $('#carousel-nav li a').removeClass('active');
	    var idx = $('#mbox .item.active').index();
	    $('#carousel-nav li a:eq(' + idx + ')').addClass('active');

	});

	/*
	 * @title Ativar todos os Modals
	 * @param Verificar se existe algum elemento 'modal' (evitar bug)
	 * @param Impedir de mostra-los ao inicilizar a página
	 */
	var $modal = $('.modal');

	if ($modal.length > 0) {
		// Rodar modal
		$modal.modal({
			show: false,
			remote: true
		});

		var $modalRemote, $modalRemoteUrl, $modalRemoteContent;

		$modalRemote = $('.modal-remote');

		// Ao clicar no link do modal
		$modalRemote.click(function(event) {
			var $this;

			$this = $(this);
			// Pegar url do elemento clicado
			$modalRemoteUrl = $this.attr('href');
			// Pegar local de 'onde jogar o conteúdo do href'
			$modalRemoteContent = $modalRemote.attr('data-target');
			$modalRemoteContent = $($modalRemoteContent).find('.modal-body');
			// Carregar url e jogar no elemento indicado pelo link
			$modalRemoteContent.load($modalRemoteUrl);

			return event.preventDefault();
		});

	}

	/*
	 * @title Cadastrar Varejistas
	 * @param Botão de 'cadastre-se' do Varejistas na Home
	 * @param Chamar Modal com formulário de @title
	 */
	var $varejistas = $('#varejistas .btn-cadastrar');

	$varejistas.click(function() {
		// Chamar modal
		
	});

	/*
	 * @description Campo SEARCH
	 * @param Quando o campo receber foco remover label
	 */
	var $field_search, $box_search, $label_search;

	$box_search = $('.search');
	$field_search = $box_search.find('input');
	$label_search = $box_search.find('label');

	// Quando acessar a página
	if($field_search.val() !== '') $label_search.hide();
	
	// Outros eventos
	$field_search.bind({
		focusin: function() { $label_search.hide(); },
		focusout: function() {
			if($field_search.val() === '') $label_search.show();
		}
	});

	/*
	 * @title Field Group
	 * @param Mostrar sub-menus ao clicar no botão
	 * @param Ao clicar no link do sub-menu, pegar o valor e jogar no campo
	 */
	function FieldGroup() {
		var $fieldGroup, $dropDown, $link, $btn, $this, value;
		
		$btn = $('.field-group .btn');
		$link = $('.dropdown-menu a');

		$btn.click(function() {
			$this = $(this);
			return $this.parent().children('.dropdown-menu').toggleClass('show');
		});

		$link.click(function(event) {
			$this = $(this);
			event.preventDefault();
			value = $this.text();
			$this.parents('.field-group').find('.field').val(value);
			return $this.parents('.dropdown-menu').toggleClass('show');
		});

		return true;
	}

	FieldGroup();

	/*
	 * @title jParallax
	 * @description Efeito Parallax Mouse nas Gotas
	 */
	var $gotas, $gotas_mouseport;

	$gotas = $('#gotas');
	$gotas_mouseport = $('.wrap.parallax-viewport');

	$gotas.plaxify({
		'xRange': 200,
		'yRange': 100
	});
	$.plax.enable();

	/*
	 * @title Parallax Scroll
	 * @param Efeito entre conteúdos/seções
	 */
	$('#main-menu .nav, #menu-nav').localScroll( 800 );

	$('#historia').parallax( '50%', 0.1 );
	$('#produtos').parallax( '50%', 0.3 );
	$('#cooperativa').parallax( '50%', 0.2 );
	$('#varejistas').parallax( '50%', 0.5 );
	$('#contato').parallax( '50%', 0.2 );

	/*
	 * Chamando a função que dispara o efeito de tabs
	 * Efeito surge em tudos elementos HTML que tem um atributo data-toggle automáticamente
	 */
	$('a[data-toggle="tab"]').on( 'shown', function (e) {
		e.target // activated tab
		e.relatedTarget // previous tab
	});
	
	/*
	 * @title Validar formulário de contato
	 */
	var $form_contato, $btn_contato;

	$form_contato = $('#form-contato');
	$btn_contato = $form_contato.find('[type="submit"]');

	$form_contato.on('submit', false);

	$btn_contato.click(function() {
		var $nome, $telefone, $assunto, $mensagem;

		$nome = $form_contato.find('#fale-nome');
		$telefone = $form_contato.find('#fale-telefone');
		$assunto = $form_contato.find('#fale-assunto');
		$mensagem = $form_contato.find('#fale-mensagem');

		if($nome.val() == '' && $assunto.val() == '' && $mensagem.val() == '') {
			$nome.focus();
			alert('Preencha todos os campos.');
			return false;
		}
		else if($nome.val() == '') {
			alert('Qual o seu nome?');
			$nome.focus();
			return false;
		}
		else if($assunto.val() == '') {
			alert('Qual o assunto do contato?');
			$assunto.focus();
			return false;
		}
		else if($mensagem.val() == '') {
			alert('Qual seria a mensagem do contato?');
			$mensagem.focus();
			return false;
		}
		else {
			if($form_contato.attr('action') != '') {
				$.post(
					$form_contato.attr('action'),
					{ nome: $nome.val(), telefone: $telefone.val(), assunto: $assunto.val(), mensagem: $mensagem.val(), form: 'contato' },
					function(data) {
						if(data)
							alert(data);
						else
							alert('Não foi possível enviar suas informações.');
					}
				);
			}
		}
	});
	
	/*
	 * @title Validar formulários do PRODUTOR
	 */
	var $form_produtor, $btn_produtor;

	$form_produtor = $('#form-produtor');
	$btn_produtor = $form_produtor.find('[type="submit"]');

	$form_produtor.on('submit', false);

	$btn_produtor.click(function() {
		var $codigo, $senha;

		$codigo = $form_produtor.find('#produtor-codigo');
		$senha = $form_produtor.find('#produtor-senha');

		if($codigo.val() == '' && $senha.val()) {
			$codigo.focus();
			alert('Preencha todos os campos.');
			return false;
		}
		else if($codigo.val() == '') {
			alert('Qual o código de acesso?');
			$codigo.focus();
			return false;
		}
		else if($senha.val() == '') {
			alert('Qual a senha de acesso?');
			$senha.focus();
			return false;
		}
		else {
			if($form_contato.attr('action') != '') {
				$.post(
					$form_produtor.attr('action'),
					{ codigo: $codigo.val(), senha: $senha.val(), form: 'produtor' },
					function(data) {
						if(data)
							alert(data);
						else
							alert('Não foi possível enviar suas informações.');
					}
				);
			}
		}
	});
	
	/*
	 * @title Validar formulários do RESTRITO
	 */
	var $form_restrito, $btn_restrito;

	$form_restrito = $('#form-restrito');
	$btn_restrito = $form_restrito.find('[type="submit"]');

	$form_restrito.on('submit', false);

	$btn_restrito.click(function() {
		var $email, $senha;

		$email = $form_restrito.find('#restrito-email');
		$senha = $form_restrito.find('#restrito-senha');

		if($email.val() == '' && $senha.val()) {
			$email.focus();
			alert('Preencha todos os campos.');
			return false;
		}
		else if($email.val() == '' || !$email.val().match(/^([a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,4}$)/i)) {
			alert('Preencha corretamente o seu e-mail.');
			$email.focus();
			return false;
		}
		else if($senha.val() == '') {
			alert('Qual a senha de acesso?');
			$senha.focus();
			return false;
		}
		else {
			if($form_contato.attr('action') != '') {
				$.post(
					$form_restrito.attr('action'),
					{ email: $email.val(), senha: $senha.val(), form: 'restrito' },
					function(data) {
						if(data)
							alert(data);
						else
							alert('Não foi possível enviar suas informações.');
					}
				);
			}
		}
	});

});